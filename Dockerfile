#Our base Image
FROM azul/zulu-openjdk:7u342-7.54.0.13

LABEL maintainer="kagoda@awamo.com"

#Set environment variables
ENV LANG C.UTF-8
ENV CATALINA_HOME /usr/local/tomcat
ENV PATH $CATALINA_HOME/bin:$PATH
ENV PATH $JAVA_HOME/bin:$PATH
RUN mkdir -p "$CATALINA_HOME"
ENV TOMCAT_VERSION 7.0.67
ENV TOMCAT_MAJOR 7

# Install dependencies
RUN apt-get update && apt-get install -y libapr1-dev libssl-dev  libtcnative-1 gcc  curl wget software-properties-common make  default-mysql-client


# Get Tomcat
RUN wget --quiet --no-cookies https://archive.apache.org/dist/tomcat/tomcat-${TOMCAT_MAJOR}/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz -O /tmp/tomcat.tgz && \
	tar xzvf /tmp/tomcat.tgz -C /usr/local \
	&& mv /usr/local/apache-tomcat-${TOMCAT_VERSION}/* /usr/local/tomcat/ \
	&& rm /tmp/tomcat.tgz \
        && mkdir -p /app .mifosx /root/.mifosx

#build the production needed APR tomcat-native library
RUN curl "https://archive.apache.org/dist/tomcat/tomcat-connectors/native/1.2.23/source/tomcat-native-1.2.23-src.tar.gz" -o ./tomcat-native-1.2.23-src.tar.gz \
	&& tar zxf ./tomcat-native-1.2.23-src.tar.gz -C . \
	&& cd tomcat-native-1.2.23-src/native/
RUN  ./tomcat-native-1.2.23-src/native/configure --with-apr=/usr/bin/apr-1-config --with-java-home=$JAVA_HOME/ \
	&& make && make install \
	&& cp -r /usr/local/apr/lib/* /usr/lib/ \
	&& java -version


#copy mifos files to the docker image

COPY ./mifosng-provider/src/main/pentahoReports /root/.mifosx/pentahoReports
COPY ./mifosng-provider/build/libs/mifosng-provider.war /usr/local/tomcat/webapps
COPY ./community-app /usr/local/tomcat/webapps/ROOT/community-app
#COPY ./community-app/test/ /usr/local/tomcat/webapps/ROOT
RUN curl -L -o /mysql-connector-java-5.1.34.jar https://repo1.maven.org/maven2/mysql/mysql-connector-java/5.1.34/mysql-connector-java-5.1.34.jar \
    && cp /mysql-connector-java-5.1.34.jar  /usr/local/tomcat/lib/
#COPY ./mysql-connector-java-5.1.47.jar /usr/local/tomcat/lib/
#COPY ./drizzle-jdbc-1.4.jar  /usr/local/tomcat/lib/
COPY ./api-docs /usr/local/tomcat/webapps/api-docs
#COPY ./libs /usr/local/tomcat/lib
RUN  cd / \
    && rm -rf /app

ENV JAVA_OPTS "-XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap"
COPY ./server.xml /usr/local/tomcat/conf/server.xml

COPY ./entrypoint.sh /usr/local/tomcat/bin/
RUN chmod +x /usr/local/tomcat/bin/entrypoint.sh

#open port 8443 for external access
EXPOSE 8443

#set working directory
WORKDIR $CATALINA_HOME

#start the entrypoint script that runs keytool to create & import TLS self-signed certificates and then start tomcat service
#ENTRYPOINT ["sh","/usr/local/tomcat/bin/entrypoint.sh"]
CMD sh /usr/local/tomcat/bin/entrypoint.sh && /usr/local/tomcat/bin/catalina.sh run
