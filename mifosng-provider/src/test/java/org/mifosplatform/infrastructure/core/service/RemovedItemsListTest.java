package org.mifosplatform.infrastructure.core.service;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

public class RemovedItemsListTest {

    @Test
    public void removeSameItemTwice() {
        RemovedItemsList<String> i = new RemovedItemsList<>();
        i.add("1");
        i.add("2");
        i.add("3");
        i.add("4");
        i.remove("3");
        i.remove("3");
        Assert.assertEquals(Arrays.asList("3"), i.getRemovedItems());
        Assert.assertEquals(Arrays.asList("1", "2", "4"), i);
    }

    @Test
    public void removeSameTwoItems() {
        RemovedItemsList<String> i = new RemovedItemsList<>();
        i.add("1");
        i.add("2");
        i.add("3");
        i.add("4");
        i.remove("3");
        i.remove("3");
        i.remove("4");
        Assert.assertEquals(Arrays.asList("3", "4"), i.getRemovedItems());
        Assert.assertEquals(Arrays.asList("1", "2"), i);
    }

    @Test
    public void removeAll() {
        RemovedItemsList<String> i = new RemovedItemsList<>();
        i.add("1");
        i.add("2");
        i.add("3");
        i.add("4");
        i.removeAll(Arrays.asList("3", "4"));
        Assert.assertEquals(Arrays.asList("3", "4"), i.getRemovedItems());
        Assert.assertEquals(Arrays.asList("1", "2"), i);
    }

    @Test
    public void testClearing() {
        RemovedItemsList<String> i = new RemovedItemsList<>();
        i.add("1");
        i.add("2");
        i.add("3");
        i.add("4");
        i.clear();
        Assert.assertEquals(Arrays.asList("1", "2", "3", "4"), i.getRemovedItems());
    }

    @Test
    public void testNewlyCreatedListIsNotInitialized() {
        {
            RemovedItemsList<String> l = new RemovedItemsList<>(1);
            Assert.assertFalse(l.isInitialized());
            l.add("1");
            Assert.assertTrue(l.isInitialized());
        }
        {
            RemovedItemsList<String> l = new RemovedItemsList<>();
            Assert.assertFalse(l.isInitialized());
            l.add("1");
            Assert.assertTrue(l.isInitialized());
        }
        {
            RemovedItemsList<String> l = new RemovedItemsList<>(Arrays.asList("2", "3"));
            Assert.assertTrue(l.isInitialized());
        }
        {
            RemovedItemsList<String> l = new RemovedItemsList<>();
            Assert.assertFalse(l.isInitialized());
            l.add(0, "1");
            Assert.assertTrue(l.isInitialized());
        }

        {
            RemovedItemsList<String> l = new RemovedItemsList<>();
            Assert.assertFalse(l.isInitialized());
            l.addAll(Arrays.asList("2", "3"));
            Assert.assertTrue(l.isInitialized());
        }

        {
            RemovedItemsList<String> l = new RemovedItemsList<>();
            Assert.assertFalse(l.isInitialized());
            l.addAll(0, Arrays.asList("2", "3"));
            Assert.assertTrue(l.isInitialized());
        }

    }

    @Test
    public void testAutoInitWithEmptyItems() {

        RemovedItemsList<String> strings = new RemovedItemsList<>();

        Assert.assertFalse(strings.isInitialized());

        strings.initIfNot(new Callable<List<String>>() {
            @Override
            public List<String> call() throws Exception {
                return null;
            }
        });

        Assert.assertTrue(strings.isInitialized());
        Assert.assertTrue(strings.isEmpty());


        //list should not be double initialized
        strings.initIfNot(new Callable<List<String>>() {
            @Override
            public List<String> call() throws Exception {
                return Arrays.asList("1", "2");
            }
        });

        Assert.assertTrue(strings.isEmpty());
        Assert.assertTrue(strings.isInitialized());


    }

    @Test
    public void testAutoInitWithItem() {

        RemovedItemsList<String> strings = new RemovedItemsList<>();

        Assert.assertFalse(strings.isInitialized());

        strings.initIfNot(new Callable<List<String>>() {
            @Override
            public List<String> call() throws Exception {
                return Arrays.asList("1", "2");
            }
        });

        Assert.assertTrue(strings.isInitialized());
        Assert.assertFalse(strings.isEmpty());

        Assert.assertEquals(strings, Arrays.asList("1", "2"));


        //list should not be double initialized
        strings.initIfNot(new Callable<List<String>>() {
            @Override
            public List<String> call() throws Exception {
                return Arrays.asList("1", "2","3");
            }
        });

        Assert.assertTrue(strings.isInitialized());
        Assert.assertFalse(strings.isEmpty());
        Assert.assertEquals(strings, Arrays.asList("1", "2"));

    }

}