package org.mifosplatform.infrastructure.core.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;

import javax.sql.DataSource;
import java.util.Map;

import static org.mockito.Mockito.*;

public class TomcatJdbcDataSourcePerTenantServiceTest {

    TomcatJdbcDataSourcePerTenantService tomcatJdbcDataSourcePerTenantService = new TomcatJdbcDataSourcePerTenantService(null);

    @Test
    public void testGetIdleTimeOut() {
        Assert.assertEquals(10_000, tomcatJdbcDataSourcePerTenantService.getIdleTimeOut(0));
        Assert.assertEquals(15_000, tomcatJdbcDataSourcePerTenantService.getIdleTimeOut(15_000));
        Assert.assertEquals(30_000, tomcatJdbcDataSourcePerTenantService.getIdleTimeOut(60_000));
    }
}