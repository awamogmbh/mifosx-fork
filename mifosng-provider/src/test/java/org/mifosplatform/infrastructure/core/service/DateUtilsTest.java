package org.mifosplatform.infrastructure.core.service;

import org.joda.time.*;
import org.junit.Test;
import org.mifosplatform.infrastructure.core.domain.MifosPlatformTenant;

import java.util.Date;

import static org.junit.Assert.*;

public class DateUtilsTest {

    @Test
    public void changSystemTime() {
        final MifosPlatformTenant peIntegrationTests = new MifosPlatformTenant(null, "peIntegrationTests", "peIntegrationTests", "UTC", null);
        ThreadLocalContextUtil.setTenant(peIntegrationTests);
        moveTime(Period.parse("P1Y").negated());
        assertEquals(LocalDate.now().minusYears(1).getYear(), DateUtils.now().getYear());


        moveTime(Period.parse("P1Y"));
        assertEquals(LocalDate.now().plusYears(1).getYear(), DateUtils.now().getYear());

        moveTime(Period.parse("PT30S"));
        assertEquals(LocalDate.now().getYear(), DateUtils.now().getYear());

        final DateTime realNow = DateUtils.__realNewDate();
        final DateTime now = DateUtils.now();
        final Period timeDiff = new Period(realNow, now);//minus(PeDateUtils.now(), noww());
        assertTrue(timeDiff.getSeconds() >= 28 && timeDiff.getSeconds() <= 30);

        DateUtils.resetTime();
        Period timeDiff2 = new Period(DateUtils.__realNewDate(), DateUtils.now());
        assertTrue(timeDiff2.getMillis() >= 0 && timeDiff2.getMillis() <= 500);

        ThreadLocalContextUtil.clearTenant();

        final String s = DateUtils.formatDateNoTimeZone(new Date(), DateUtils.DD_MMM_YYYY_HH_MM_SS_AA_UNDERSCORE);
        assertNotNull(s, "we are supposed to form with no time zone");

    }

    @Test
    public void changSystemTimeOtherTenant() {

        final MifosPlatformTenant peIntegrationTests = new MifosPlatformTenant(null, "peIntegrationTests", "peIntegrationTests", "UTC", null);
        ThreadLocalContextUtil.setTenant(peIntegrationTests);
        moveTime(Period.parse("P1Y").negated());
        {
            assertEquals(LocalDate.now().minusYears(1).getYear(), DateUtils.now().getYear());
            final Seconds seconds = Seconds.secondsBetween(DateUtils.__realNewDate(), DateUtils.now());
            assertFalse(seconds.getSeconds() >= 0 && seconds.getSeconds() <= 1);//we should have the real date
        }


        {
            //switch to peIntegrationTests2
            final MifosPlatformTenant peIntegrationTests2 = new MifosPlatformTenant(null, "peIntegrationTests2", "peIntegrationTests2", "UTC", null);
            ThreadLocalContextUtil.setTenant(peIntegrationTests2);

            assertNotEquals(LocalDate.now().minusYears(1).getYear(), DateUtils.now().getYear());
            Period timeDiff2 = new Period(DateUtils.__realNewDate(), DateUtils.now());
            final Seconds seconds = Seconds.secondsBetween(DateUtils.__realNewDate(), DateUtils.now());
            assertTrue(seconds.getSeconds() >= 0 && seconds.getSeconds() <= 1);//we should have the real date
            assertTrue(timeDiff2.getMillis() >= 0 && timeDiff2.getMillis() <= 500);//we should have the real date
        }


        {
            //switch to ITEST
            ThreadLocalContextUtil.setTenant(peIntegrationTests);

            assertEquals(LocalDate.now().minusYears(1).getYear(), DateUtils.now().getYear());
            final Seconds seconds = Seconds.secondsBetween(DateUtils.__realNewDate(), DateUtils.now());
            assertFalse(seconds.getSeconds() >= 0 && seconds.getSeconds() <= 1);//we should have the real date
        }


    }

    private void moveTime(Period period) {
        final DateTime plus = DateUtils.__realNewDate().plus(period);
        DateUtils.changSystemTime(plus.toDate());
    }

}