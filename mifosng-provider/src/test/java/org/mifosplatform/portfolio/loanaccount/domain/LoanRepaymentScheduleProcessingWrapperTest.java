package org.mifosplatform.portfolio.loanaccount.domain;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mifosplatform.CheckedRunnable;
import org.mifosplatform.TestUtils;
import org.mifosplatform.infrastructure.core.service.DateUtils;
import org.mifosplatform.organisation.monetary.domain.MonetaryCurrency;
import org.mifosplatform.organisation.monetary.domain.Money;
import org.mifosplatform.organisation.monetary.domain.MoneyHelper;
import org.mifosplatform.portfolio.charge.domain.Charge;
import org.mifosplatform.portfolio.charge.domain.ChargeCalculationType;
import org.mifosplatform.portfolio.charge.domain.ChargePaymentMode;
import org.mifosplatform.portfolio.charge.domain.ChargeTimeType;
import org.mifosplatform.portfolio.loanproduct.domain.LoanProductRelatedDetail;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class LoanRepaymentScheduleProcessingWrapperTest {

    LoanRepaymentScheduleProcessingWrapper loanRepaymentScheduleProcessingWrapper = new LoanRepaymentScheduleProcessingWrapper();
    AtomicLong counter = new AtomicLong(0);

    @Before
    public void setUp() {
        MoneyHelper.overrideGlobalRoundingMode(RoundingMode.HALF_EVEN);
    }

    @Test
    public void testGetActiveOverduePenaltiesForInstallment() {

        //create empty list of loan charges
        List<LoanCharge> loanCharges = new ArrayList<>();

        LoanRepaymentScheduleInstallment instalment1 = createInstallment();
        LoanCharge loanCharge1 = createLoanCharge(null, ChargeTimeType.OVERDUE_INSTALLMENT, ChargeCalculationType.INVALID);
        {
            LoanOverdueInstallmentCharge overdueInstallmentCharge = new LoanOverdueInstallmentCharge(loanCharge1, instalment1, 2);
            loanCharge1.updateOverdueInstallmentCharge(overdueInstallmentCharge);
            loanCharges.add(loanCharge1);
        }

        LoanCharge loanCharge2 = createLoanCharge(null, ChargeTimeType.OVERDUE_INSTALLMENT, ChargeCalculationType.INVALID);
        {
            LoanOverdueInstallmentCharge overdueInstallmentCharge = new LoanOverdueInstallmentCharge(loanCharge2, instalment1, 2);
            loanCharge2.updateOverdueInstallmentCharge(overdueInstallmentCharge);
            loanCharges.add(loanCharge2);
        }

        {
            LoanCharge loanCharge = createLoanCharge(null, ChargeTimeType.INVALID, ChargeCalculationType.INVALID);
            loanCharge.updateIsPenalty(false);
            LoanOverdueInstallmentCharge overdueInstallmentCharge = new LoanOverdueInstallmentCharge(loanCharge, instalment1, 2);
            loanCharge.updateOverdueInstallmentCharge(overdueInstallmentCharge);
            loanCharges.add(loanCharge);
        }

        final LoanRepaymentScheduleInstallment installment = loanRepaymentScheduleProcessingWrapper.extractInstallment(loanCharges.get(0));

        List<LoanCharge> result = loanRepaymentScheduleProcessingWrapper.getActiveOverduePenaltiesForInstallment(loanCharges, instalment1, installment.getFromDate(), instalment1.getDueDate());
        Assert.assertEquals(Arrays.<LoanCharge>asList(loanCharge1, loanCharge2), result);
    }


    @Test
    public void testCheckThatItIsASupportedAwamoCharge() {
        isNotAwamoCharge(ChargeTimeType.INVALID, ChargeCalculationType.INVALID);
        isNotAwamoCharge(ChargeTimeType.INVALID, ChargeCalculationType.FLAT);
        isNotAwamoCharge(ChargeTimeType.INVALID, ChargeCalculationType.PERCENT_OF_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.INVALID, ChargeCalculationType.PERCENT_OF_AMOUNT_AND_INTEREST);
        isNotAwamoCharge(ChargeTimeType.INVALID, ChargeCalculationType.PERCENT_OF_INTEREST);
        isNotAwamoCharge(ChargeTimeType.INVALID, ChargeCalculationType.PERCENT_OF_DISBURSEMENT_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.DISBURSEMENT, ChargeCalculationType.INVALID);
        isNotAwamoCharge(ChargeTimeType.DISBURSEMENT, ChargeCalculationType.FLAT);
        isNotAwamoCharge(ChargeTimeType.DISBURSEMENT, ChargeCalculationType.PERCENT_OF_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.DISBURSEMENT, ChargeCalculationType.PERCENT_OF_AMOUNT_AND_INTEREST);
        isNotAwamoCharge(ChargeTimeType.DISBURSEMENT, ChargeCalculationType.PERCENT_OF_INTEREST);
        isNotAwamoCharge(ChargeTimeType.DISBURSEMENT, ChargeCalculationType.PERCENT_OF_DISBURSEMENT_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.SPECIFIED_DUE_DATE, ChargeCalculationType.INVALID);
        isNotAwamoCharge(ChargeTimeType.SPECIFIED_DUE_DATE, ChargeCalculationType.FLAT);
        isNotAwamoCharge(ChargeTimeType.SPECIFIED_DUE_DATE, ChargeCalculationType.PERCENT_OF_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.SPECIFIED_DUE_DATE, ChargeCalculationType.PERCENT_OF_AMOUNT_AND_INTEREST);
        isNotAwamoCharge(ChargeTimeType.SPECIFIED_DUE_DATE, ChargeCalculationType.PERCENT_OF_INTEREST);
        isNotAwamoCharge(ChargeTimeType.SPECIFIED_DUE_DATE, ChargeCalculationType.PERCENT_OF_DISBURSEMENT_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.SAVINGS_ACTIVATION, ChargeCalculationType.INVALID);
        isNotAwamoCharge(ChargeTimeType.SAVINGS_ACTIVATION, ChargeCalculationType.FLAT);
        isNotAwamoCharge(ChargeTimeType.SAVINGS_ACTIVATION, ChargeCalculationType.PERCENT_OF_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.SAVINGS_ACTIVATION, ChargeCalculationType.PERCENT_OF_AMOUNT_AND_INTEREST);
        isNotAwamoCharge(ChargeTimeType.SAVINGS_ACTIVATION, ChargeCalculationType.PERCENT_OF_INTEREST);
        isNotAwamoCharge(ChargeTimeType.SAVINGS_ACTIVATION, ChargeCalculationType.PERCENT_OF_DISBURSEMENT_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.SAVINGS_CLOSURE, ChargeCalculationType.INVALID);
        isNotAwamoCharge(ChargeTimeType.SAVINGS_CLOSURE, ChargeCalculationType.FLAT);
        isNotAwamoCharge(ChargeTimeType.SAVINGS_CLOSURE, ChargeCalculationType.PERCENT_OF_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.SAVINGS_CLOSURE, ChargeCalculationType.PERCENT_OF_AMOUNT_AND_INTEREST);
        isNotAwamoCharge(ChargeTimeType.SAVINGS_CLOSURE, ChargeCalculationType.PERCENT_OF_INTEREST);
        isNotAwamoCharge(ChargeTimeType.SAVINGS_CLOSURE, ChargeCalculationType.PERCENT_OF_DISBURSEMENT_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.WITHDRAWAL_FEE, ChargeCalculationType.INVALID);
        isNotAwamoCharge(ChargeTimeType.WITHDRAWAL_FEE, ChargeCalculationType.FLAT);
        isNotAwamoCharge(ChargeTimeType.WITHDRAWAL_FEE, ChargeCalculationType.PERCENT_OF_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.WITHDRAWAL_FEE, ChargeCalculationType.PERCENT_OF_AMOUNT_AND_INTEREST);
        isNotAwamoCharge(ChargeTimeType.WITHDRAWAL_FEE, ChargeCalculationType.PERCENT_OF_INTEREST);
        isNotAwamoCharge(ChargeTimeType.WITHDRAWAL_FEE, ChargeCalculationType.PERCENT_OF_DISBURSEMENT_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.ANNUAL_FEE, ChargeCalculationType.INVALID);
        isNotAwamoCharge(ChargeTimeType.ANNUAL_FEE, ChargeCalculationType.FLAT);
        isNotAwamoCharge(ChargeTimeType.ANNUAL_FEE, ChargeCalculationType.PERCENT_OF_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.ANNUAL_FEE, ChargeCalculationType.PERCENT_OF_AMOUNT_AND_INTEREST);
        isNotAwamoCharge(ChargeTimeType.ANNUAL_FEE, ChargeCalculationType.PERCENT_OF_INTEREST);
        isNotAwamoCharge(ChargeTimeType.ANNUAL_FEE, ChargeCalculationType.PERCENT_OF_DISBURSEMENT_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.MONTHLY_FEE, ChargeCalculationType.INVALID);
        isNotAwamoCharge(ChargeTimeType.MONTHLY_FEE, ChargeCalculationType.FLAT);
        isNotAwamoCharge(ChargeTimeType.MONTHLY_FEE, ChargeCalculationType.PERCENT_OF_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.MONTHLY_FEE, ChargeCalculationType.PERCENT_OF_AMOUNT_AND_INTEREST);
        isNotAwamoCharge(ChargeTimeType.MONTHLY_FEE, ChargeCalculationType.PERCENT_OF_INTEREST);
        isNotAwamoCharge(ChargeTimeType.MONTHLY_FEE, ChargeCalculationType.PERCENT_OF_DISBURSEMENT_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.INSTALMENT_FEE, ChargeCalculationType.INVALID);
        isNotAwamoCharge(ChargeTimeType.INSTALMENT_FEE, ChargeCalculationType.FLAT);
        isNotAwamoCharge(ChargeTimeType.INSTALMENT_FEE, ChargeCalculationType.PERCENT_OF_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.INSTALMENT_FEE, ChargeCalculationType.PERCENT_OF_AMOUNT_AND_INTEREST);
        isNotAwamoCharge(ChargeTimeType.INSTALMENT_FEE, ChargeCalculationType.PERCENT_OF_INTEREST);
        isNotAwamoCharge(ChargeTimeType.INSTALMENT_FEE, ChargeCalculationType.PERCENT_OF_DISBURSEMENT_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.OVERDUE_INSTALLMENT, ChargeCalculationType.PERCENT_OF_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.OVERDUE_INSTALLMENT, ChargeCalculationType.PERCENT_OF_AMOUNT_AND_INTEREST);
        isNotAwamoCharge(ChargeTimeType.OVERDUE_INSTALLMENT, ChargeCalculationType.PERCENT_OF_INTEREST);
        isNotAwamoCharge(ChargeTimeType.OVERDUE_INSTALLMENT, ChargeCalculationType.PERCENT_OF_DISBURSEMENT_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.OVERDRAFT_FEE, ChargeCalculationType.INVALID);
        isNotAwamoCharge(ChargeTimeType.OVERDRAFT_FEE, ChargeCalculationType.FLAT);
        isNotAwamoCharge(ChargeTimeType.OVERDRAFT_FEE, ChargeCalculationType.PERCENT_OF_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.OVERDRAFT_FEE, ChargeCalculationType.PERCENT_OF_AMOUNT_AND_INTEREST);
        isNotAwamoCharge(ChargeTimeType.OVERDRAFT_FEE, ChargeCalculationType.PERCENT_OF_INTEREST);
        isNotAwamoCharge(ChargeTimeType.OVERDRAFT_FEE, ChargeCalculationType.PERCENT_OF_DISBURSEMENT_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.WEEKLY_FEE, ChargeCalculationType.INVALID);
        isNotAwamoCharge(ChargeTimeType.WEEKLY_FEE, ChargeCalculationType.FLAT);
        isNotAwamoCharge(ChargeTimeType.WEEKLY_FEE, ChargeCalculationType.PERCENT_OF_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.WEEKLY_FEE, ChargeCalculationType.PERCENT_OF_AMOUNT_AND_INTEREST);
        isNotAwamoCharge(ChargeTimeType.WEEKLY_FEE, ChargeCalculationType.PERCENT_OF_INTEREST);
        isNotAwamoCharge(ChargeTimeType.WEEKLY_FEE, ChargeCalculationType.PERCENT_OF_DISBURSEMENT_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.TRANCHE_DISBURSEMENT, ChargeCalculationType.INVALID);
        isNotAwamoCharge(ChargeTimeType.TRANCHE_DISBURSEMENT, ChargeCalculationType.FLAT);
        isNotAwamoCharge(ChargeTimeType.TRANCHE_DISBURSEMENT, ChargeCalculationType.PERCENT_OF_AMOUNT);
        isNotAwamoCharge(ChargeTimeType.TRANCHE_DISBURSEMENT, ChargeCalculationType.PERCENT_OF_AMOUNT_AND_INTEREST);
        isNotAwamoCharge(ChargeTimeType.TRANCHE_DISBURSEMENT, ChargeCalculationType.PERCENT_OF_INTEREST);
        isNotAwamoCharge(ChargeTimeType.TRANCHE_DISBURSEMENT, ChargeCalculationType.PERCENT_OF_DISBURSEMENT_AMOUNT);

        isAwamoCharge(ChargeTimeType.OVERDUE_INSTALLMENT, ChargeCalculationType.INVALID);
        isAwamoCharge(ChargeTimeType.OVERDUE_INSTALLMENT, ChargeCalculationType.FLAT);
    }

    private void isNotAwamoCharge(final ChargeTimeType chargeTimeType, final ChargeCalculationType invalid) {
        TestUtils.shouldFail(new CheckedRunnable() {
            @Override
            public void run() {
                loanRepaymentScheduleProcessingWrapper.checkThatItIsASupportedAwamoCharge(createLoanCharge(null, chargeTimeType, invalid));
            }
        });
    }

    private void isAwamoCharge(final ChargeTimeType chargeTimeType, final ChargeCalculationType invalid) {
        TestUtils.shouldNotFail(new CheckedRunnable() {
            @Override
            public void run() {
                loanRepaymentScheduleProcessingWrapper.checkThatItIsASupportedAwamoCharge(createLoanCharge(null, chargeTimeType, invalid));
            }
        });
    }

    private LoanRepaymentScheduleInstallment createInstallment() {
        final LoanRepaymentScheduleInstallment loanRepaymentScheduleInstallment = new LoanRepaymentScheduleInstallment(null,
                null,
                DateUtils.nowAsLocalDate(),
                DateUtils.nowAsLocalDate(),
                new BigDecimal(0),
                new BigDecimal(0),
                new BigDecimal(0),
                new BigDecimal(0),
                true);

        loanRepaymentScheduleInstallment.setId(counter.incrementAndGet());

        return loanRepaymentScheduleInstallment;
    }


    private LoanCharge createLoanCharge(BigDecimal chargeAmount,
                                        ChargeTimeType chargeTimeType,
                                        ChargeCalculationType chargeCalculation) {

        final Charge chargeDefinition = createChargeDefinition(chargeTimeType, chargeCalculation);

        Loan loan = new Loan();
        loan.updateLoanRepaymentScheduleDetail(new LoanProductRelatedDetail());
        loan.updateCurrency(new MonetaryCurrency("UGX", 2, null));

        final LoanCharge loanCharge = new LoanCharge(loan, chargeDefinition,
                null,
                chargeAmount,
                chargeTimeType,
                chargeCalculation,
                DateUtils.nowAsLocalDate(),
                ChargePaymentMode.REGULAR,
                0,
                new BigDecimal(0));
        loanCharge.setId(counter.incrementAndGet());
        return loanCharge;
    }

    private Charge createChargeDefinition(ChargeTimeType chargeTimeType, ChargeCalculationType chargeCalculation) {
        final Charge chargeDefinition = new Charge();
        chargeDefinition.updateIsPenalty(true);
        chargeDefinition.updateChargeTimeType(chargeTimeType);
        chargeDefinition.updateChargeCalculation(chargeCalculation);
        chargeDefinition.updateName("Penalty Charge");
        return chargeDefinition;
    }

    @Test
    public void testIsActivePenalty() {
        {
            boolean result = loanRepaymentScheduleProcessingWrapper.isActivePenalty(createLoanCharge(null, ChargeTimeType.INVALID, ChargeCalculationType.INVALID));
            Assert.assertTrue(result);
        }
        {
            final LoanCharge loanCharge = createLoanCharge(null, ChargeTimeType.INVALID, ChargeCalculationType.INVALID);
            loanCharge.updateIsPenalty(false);
            boolean result = loanRepaymentScheduleProcessingWrapper.isActivePenalty(loanCharge);
            Assert.assertFalse(result);
        }
        {
            final LoanCharge loanCharge = createLoanCharge(null, ChargeTimeType.INVALID, ChargeCalculationType.INVALID);
            loanCharge.updateIsPenalty(true);
            loanCharge.updateActive(false);
            boolean result = loanRepaymentScheduleProcessingWrapper.isActivePenalty(loanCharge);
            Assert.assertFalse(result);
        }

    }

    @Test
    public void testExtractInstallment() {
        final LoanCharge loanCharge = createLoanCharge(null, ChargeTimeType.INVALID, ChargeCalculationType.INVALID);
        final LoanRepaymentScheduleInstallment installment = createInstallment();
        LoanOverdueInstallmentCharge overdueInstallmentCharge = new LoanOverdueInstallmentCharge(loanCharge, installment, 2);
        loanCharge.updateOverdueInstallmentCharge(overdueInstallmentCharge);
        LoanRepaymentScheduleInstallment result = loanRepaymentScheduleProcessingWrapper.extractInstallment(loanCharge);
        Assert.assertEquals(installment, result);
    }

    @Test
    public void testExtractInstallmentThrowsExceptionIfNoLoanChargeIsSet() {
        final LoanCharge loanCharge = createLoanCharge(null, ChargeTimeType.INVALID, ChargeCalculationType.INVALID);
        final LoanRepaymentScheduleInstallment installment = createInstallment();

        final Throwable throwable = TestUtils.shouldFail(new CheckedRunnable() {
            @Override
            public void run() {
                loanRepaymentScheduleProcessingWrapper.extractInstallment(loanCharge);
            }
        });
        Assert.assertTrue(throwable.getMessage().contains("Overdue installment charge is null for penalty charge"));
    }

    @Test
    public void testIsSameInstallment() {
        final LoanRepaymentScheduleInstallment installment = createInstallment();
        final LoanRepaymentScheduleInstallment installment1 = createInstallment();

        Assert.assertFalse(loanRepaymentScheduleProcessingWrapper.isSameInstallment(installment, installment1));
        Assert.assertTrue(loanRepaymentScheduleProcessingWrapper.isSameInstallment(installment, installment));
    }

    @Test
    public void testSummationOfPenalties() {
        //create empty list of loan charges
        List<LoanCharge> loanCharges = new ArrayList<>();

        LoanRepaymentScheduleInstallment instalment1 = createInstallment();
        LoanCharge loanCharge1 = createLoanCharge(new BigDecimal("2"), ChargeTimeType.OVERDUE_INSTALLMENT, ChargeCalculationType.INVALID);

        {
            LoanOverdueInstallmentCharge overdueInstallmentCharge = new LoanOverdueInstallmentCharge(loanCharge1, instalment1, 2);
            loanCharge1.updateOverdueInstallmentCharge(overdueInstallmentCharge);
            loanCharges.add(loanCharge1);
        }

        LoanCharge loanCharge2 = createLoanCharge(new BigDecimal("3"), ChargeTimeType.OVERDUE_INSTALLMENT, ChargeCalculationType.INVALID);
        {
            LoanOverdueInstallmentCharge overdueInstallmentCharge = new LoanOverdueInstallmentCharge(loanCharge2, instalment1, 2);
            loanCharge2.updateOverdueInstallmentCharge(overdueInstallmentCharge);
            loanCharges.add(loanCharge2);

        }

        {
            LoanCharge loanCharge = createLoanCharge(new BigDecimal("4"), ChargeTimeType.INVALID, ChargeCalculationType.INVALID);
            loanCharge.updateIsPenalty(false);
            LoanOverdueInstallmentCharge overdueInstallmentCharge = new LoanOverdueInstallmentCharge(loanCharge, instalment1, 2);
            loanCharge.updateOverdueInstallmentCharge(overdueInstallmentCharge);
            loanCharges.add(loanCharge);
        }

        final Money installment =
                loanRepaymentScheduleProcessingWrapper.cumulativePenaltyChargesDueWithin(
                        DateUtils.nowAsLocalDate(), DateUtils.nowAsLocalDate(), new HashSet<LoanCharge>(loanCharges), loanCharge1.getLoan().getCurrency(), instalment1);


        Assert.assertEquals(5, installment.getAmount().intValue());

    }

    @Test
    public void testSummationOfWaivePenalties() {
        //create empty list of loan charges
        List<LoanCharge> loanCharges = new ArrayList<>();

        LoanRepaymentScheduleInstallment instalment1 = createInstallment();
        LoanCharge loanCharge1 = createLoanCharge(new BigDecimal("10"), ChargeTimeType.OVERDUE_INSTALLMENT, ChargeCalculationType.INVALID);
        loanCharge1.setAmountWaived(new BigDecimal("2"));

        {
            LoanOverdueInstallmentCharge overdueInstallmentCharge = new LoanOverdueInstallmentCharge(loanCharge1, instalment1, 2);
            loanCharge1.updateOverdueInstallmentCharge(overdueInstallmentCharge);
            loanCharges.add(loanCharge1);
        }

        LoanCharge loanCharge2 = createLoanCharge(new BigDecimal("20"), ChargeTimeType.OVERDUE_INSTALLMENT, ChargeCalculationType.INVALID);
        loanCharge2.setAmountWaived(new BigDecimal("5"));

        {
            LoanOverdueInstallmentCharge overdueInstallmentCharge = new LoanOverdueInstallmentCharge(loanCharge2, instalment1, 2);
            loanCharge2.updateOverdueInstallmentCharge(overdueInstallmentCharge);
            loanCharges.add(loanCharge2);

        }

        {
            LoanCharge loanCharge = createLoanCharge(new BigDecimal("30"), ChargeTimeType.INVALID, ChargeCalculationType.INVALID);
            loanCharge.setAmountWaived(new BigDecimal("4"));
            loanCharge.updateIsPenalty(false);
            LoanOverdueInstallmentCharge overdueInstallmentCharge = new LoanOverdueInstallmentCharge(loanCharge, instalment1, 2);
            loanCharge.updateOverdueInstallmentCharge(overdueInstallmentCharge);
            loanCharges.add(loanCharge);
        }

        final Money installment =
                loanRepaymentScheduleProcessingWrapper.cumulativePenaltyChargesWaivedWithin(
                        DateUtils.nowAsLocalDate(), DateUtils.nowAsLocalDate(), new HashSet<LoanCharge>(loanCharges), loanCharge1.getLoan().getCurrency(), instalment1);


        Assert.assertEquals(7, installment.getAmount().intValue());

    }

    @Test
    public void testSummationOfWrittenOffPenalties() {
        //create empty list of loan charges
        List<LoanCharge> loanCharges = new ArrayList<>();

        LoanRepaymentScheduleInstallment instalment1 = createInstallment();
        LoanCharge loanCharge1 = createLoanCharge(new BigDecimal("10"), ChargeTimeType.OVERDUE_INSTALLMENT, ChargeCalculationType.INVALID);
        loanCharge1.setAmountWrittenOff(new BigDecimal("7"));

        {
            LoanOverdueInstallmentCharge overdueInstallmentCharge = new LoanOverdueInstallmentCharge(loanCharge1, instalment1, 2);
            loanCharge1.updateOverdueInstallmentCharge(overdueInstallmentCharge);
            loanCharges.add(loanCharge1);
        }

        LoanCharge loanCharge2 = createLoanCharge(new BigDecimal("20"), ChargeTimeType.OVERDUE_INSTALLMENT, ChargeCalculationType.INVALID);
        loanCharge2.setAmountWrittenOff(new BigDecimal("5"));

        {
            LoanOverdueInstallmentCharge overdueInstallmentCharge = new LoanOverdueInstallmentCharge(loanCharge2, instalment1, 2);
            loanCharge2.updateOverdueInstallmentCharge(overdueInstallmentCharge);
            loanCharges.add(loanCharge2);

        }

        {
            LoanCharge loanCharge = createLoanCharge(new BigDecimal("30"), ChargeTimeType.INVALID, ChargeCalculationType.INVALID);
            loanCharge.setAmountWrittenOff(new BigDecimal("4"));
            loanCharge.updateIsPenalty(false);
            LoanOverdueInstallmentCharge overdueInstallmentCharge = new LoanOverdueInstallmentCharge(loanCharge, instalment1, 2);
            loanCharge.updateOverdueInstallmentCharge(overdueInstallmentCharge);
            loanCharges.add(loanCharge);
        }

        final Money installment =
                loanRepaymentScheduleProcessingWrapper.cumulativePenaltyChargesWrittenOffWithin(
                        DateUtils.nowAsLocalDate(), DateUtils.nowAsLocalDate(), new HashSet<LoanCharge>(loanCharges), loanCharge1.getLoan().getCurrency(), instalment1);


        Assert.assertEquals(12, installment.getAmount().intValue());

    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme