package org.mifosplatform.portfolio.loanaccount.domain;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.junit.Test;
import org.mifosplatform.organisation.monetary.domain.MonetaryCurrency;
import org.mifosplatform.organisation.monetary.domain.Money;
import org.mifosplatform.organisation.monetary.domain.MoneyHelper;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LoanChargeTest {


    @Test
    public void determineIfNotFullyPaid() {
        MoneyHelper.overrideGlobalRoundingMode(RoundingMode.HALF_EVEN);
        MonetaryCurrency currency = new MonetaryCurrency("UGX",5,1);
        {
            LoanCharge charge = new LoanCharge();
            charge.setAmount(BigDecimal.TEN);
            charge.setAmountPaid(BigDecimal.ZERO);
            charge.setAmountWaived(BigDecimal.ZERO);
            charge.setAmountWaived(BigDecimal.ZERO);

            assertTrue(charge.determineIfNotFullyPaid(currency));
            assertFalse(charge.determineIfFullyPaid());
        }
        {
            //fully paid
            LoanCharge charge = new LoanCharge();
            charge.setAmount(BigDecimal.TEN);
            charge.setAmountPaid(BigDecimal.TEN);
            charge.setAmountWaived(BigDecimal.ZERO);
            charge.setAmountWrittenOff(BigDecimal.ZERO);

            assertFalse(charge.determineIfNotFullyPaid(currency));
            assertTrue(charge.determineIfFullyPaid());
        }
        {
            //half paid half waived
            LoanCharge charge = new LoanCharge();
            charge.setAmount(BigDecimal.TEN);
            charge.setAmountPaid(new BigDecimal(5));
            charge.setAmountWaived(new BigDecimal(5));
            charge.setAmountWrittenOff(BigDecimal.ZERO);

            assertFalse(charge.determineIfNotFullyPaid(currency));
            assertTrue(charge.determineIfFullyPaid());
        }
        {
            //half paid half waived hald written off
            LoanCharge charge = new LoanCharge();
            charge.setAmount(BigDecimal.TEN);
            charge.setAmountPaid(new BigDecimal(4));
            charge.setAmountWaived(new BigDecimal(3));
            charge.setAmountWrittenOff(new BigDecimal(3));

            assertFalse(charge.determineIfNotFullyPaid(currency));
            assertTrue(charge.determineIfFullyPaid());
        }
        {
            //not complete
            LoanCharge charge = new LoanCharge();
            charge.setAmount(BigDecimal.TEN);
            charge.setAmountPaid(new BigDecimal(4));
            charge.setAmountWaived(new BigDecimal(3));
            charge.setAmountWrittenOff(new BigDecimal(2));

            assertTrue(charge.determineIfNotFullyPaid(currency));
            assertFalse(charge.determineIfFullyPaid());
        }
        {
            //complete
            LoanCharge charge = new LoanCharge();
            charge.setAmount(BigDecimal.TEN);
            charge.setAmountPaid(new BigDecimal(4));
            charge.setAmountWaived(new BigDecimal(3));
            charge.setAmountWrittenOff(new BigDecimal("3.002"));

            assertFalse(charge.determineIfNotFullyPaid(currency));
            assertTrue(charge.determineIfFullyPaid());
        }
        {
            MonetaryCurrency USD = new MonetaryCurrency("USD",2,50);

            //complete
            LoanCharge charge = new LoanCharge();
            charge.setAmount(new BigDecimal("3.333333"));
            charge.setAmountPaid(new BigDecimal("3.33"));
            charge.setAmountWaived(null);
            charge.setAmountWrittenOff(null);

            assertFalse(charge.determineIfNotFullyPaid(USD));
            assertTrue(charge.determineIfFullyPaid(USD));
            assertFalse(charge.determineIfFullyPaid());
        }

        {
            MonetaryCurrency USD = new MonetaryCurrency("USD",2,50);

            //complete
            LoanCharge charge = new LoanCharge();
            charge.setAmount(new BigDecimal("3.333333"));
            charge.setAmountPaid(new BigDecimal("1.33333"));
            charge.setAmountWaived(new BigDecimal("2"));
            charge.setAmountWrittenOff(null);

            assertFalse(charge.determineIfNotFullyPaid(USD));
            assertTrue(charge.determineIfFullyPaid(USD));
            assertFalse(charge.determineIfFullyPaid());
        }


    }

    @Test
    public void occursOnDayFromAndUpToAndIncluding() {

        LoanCharge charge = new LoanCharge();
        final LocalDate _19th = LocalDate.parse("19/Jul/2020", DateTimeFormat.forPattern("dd/MMM/yyyy"));
        final LocalDate _20th = LocalDate.parse("20/Jul/2020", DateTimeFormat.forPattern("dd/MMM/yyyy"));
        final LocalDate _21st = LocalDate.parse("21/Jul/2020", DateTimeFormat.forPattern("dd/MMM/yyyy"));
        final LocalDate _30th = LocalDate.parse("30/Jul/2020", DateTimeFormat.forPattern("dd/MMM/yyyy"));
        final LocalDate _31th = LocalDate.parse("31/Jul/2020", DateTimeFormat.forPattern("dd/MMM/yyyy"));

        assertTrue(charge.occursOnDayFromAndUpToAndIncluding(_20th, _20th, _20th));
        assertTrue(charge.occursOnDayFromAndUpToAndIncluding(_20th, _30th, _21st));
        assertTrue(charge.occursOnDayFromAndUpToAndIncluding(_20th, _30th, _30th));

        assertFalse(charge.occursOnDayFromAndUpToAndIncluding(_20th, _30th, _19th));
        assertFalse(charge.occursOnDayFromAndUpToAndIncluding(_20th, _30th, _20th));
        assertFalse(charge.occursOnDayFromAndUpToAndIncluding(_20th, _30th, _31th));
    }

    @Test
    public void testUpdatingLoanChargeWIthDecimals() {

        MoneyHelper.overrideGlobalRoundingMode(RoundingMode.HALF_EVEN);

        {
            MonetaryCurrency currency = new MonetaryCurrency("UGX", 5, 1);
            LoanCharge charge = new LoanCharge();
            charge.setAmount(BigDecimal.TEN);
            charge.setAmountPaid(BigDecimal.ZERO);
            charge.setAmountWaived(BigDecimal.ZERO);
            charge.setAmountWaived(BigDecimal.ZERO);
            charge.setAmountOutstanding(BigDecimal.ZERO);

            charge.updatePaidAmountBy(Money.of(currency, BigDecimal.TEN), null, null);


            assertFalse(charge.determineIfNotFullyPaid(currency));
            assertTrue(charge.determineIfFullyPaid());
            assertTrue(charge.determineIfFullyPaid(currency));
        }

        {
            MonetaryCurrency currency = new MonetaryCurrency("UGX", 0, 1);
            LoanCharge charge = new LoanCharge();
            charge.setAmount(new BigDecimal("10.2"));
            charge.setAmountPaid(BigDecimal.ZERO);
            charge.setAmountWaived(BigDecimal.ZERO);
            charge.setAmountWaived(BigDecimal.ZERO);
            charge.setAmountOutstanding(BigDecimal.ZERO);

            charge.updatePaidAmountBy(Money.of(currency, new BigDecimal("11")), null, null);


            assertFalse(charge.determineIfNotFullyPaid(currency));
            assertFalse(charge.determineIfFullyPaid());//not fully paid when you check the actual values
            assertTrue(charge.determineIfFullyPaid(currency));
        }

    }

}