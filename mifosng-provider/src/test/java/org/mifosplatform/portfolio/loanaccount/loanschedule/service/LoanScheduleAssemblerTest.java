package org.mifosplatform.portfolio.loanaccount.loanschedule.service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;
import org.mifosplatform.infrastructure.core.serialization.FromJsonHelper;
import org.mifosplatform.portfolio.common.domain.PeriodFrequencyType;
import org.mifosplatform.portfolio.loanaccount.loanschedule.domain.AprCalculator;

import java.math.BigDecimal;
import java.util.Locale;

import static org.junit.Assert.*;

public class LoanScheduleAssemblerTest {

    @Test
    public void calculateAnnualInterest() {

        FromJsonHelper fromJsonHelper = new FromJsonHelper();
        AprCalculator aprCalculator = new AprCalculator();

        LoanScheduleAssembler assembler = new LoanScheduleAssembler(fromJsonHelper, null, null, null, aprCalculator
                , null, null, null, null, null, null, null, null);


        {
            JsonObject element = new JsonObject();
            element.addProperty("interestRatePerPeriod", 0.6);
            element.addProperty("interestRateFrequencyType", 1);//1 for weeks
            element.addProperty("locale", "en_US");

            System.out.println(Locale.getDefault());

            Pair<BigDecimal, PeriodFrequencyType> bigDecimalPeriodFrequencyTypePair = assembler.calculateAnnualInterest(element);

            assertEquals(bigDecimalPeriodFrequencyTypePair.getLeft(), new BigDecimal("31.2"));
            assertEquals(PeriodFrequencyType.WEEKS, bigDecimalPeriodFrequencyTypePair.getRight());
        }

        {
            JsonObject element = new JsonObject();
            element.addProperty("interestRatePerPeriod", 0.6);
            element.addProperty("interestRateFrequencyType", 3);//1 for years
            element.addProperty("locale", "en_US");

            System.out.println(Locale.getDefault());

            Pair<BigDecimal, PeriodFrequencyType> bigDecimalPeriodFrequencyTypePair = assembler.calculateAnnualInterest(element);

            assertEquals(bigDecimalPeriodFrequencyTypePair.getLeft(), new BigDecimal("0.6"));
            assertEquals(PeriodFrequencyType.YEARS, bigDecimalPeriodFrequencyTypePair.getRight());
        }
        {
            JsonObject element = new JsonObject();
            element.addProperty("interestRatePerPeriod", 0.6);
            element.addProperty("interestRateFrequencyType", 2);//1 for months
            element.addProperty("locale", "en_US");

            System.out.println(Locale.getDefault());

            Pair<BigDecimal, PeriodFrequencyType> bigDecimalPeriodFrequencyTypePair = assembler.calculateAnnualInterest(element);

            assertEquals(bigDecimalPeriodFrequencyTypePair.getLeft(), new BigDecimal("7.2"));
            assertEquals(PeriodFrequencyType.MONTHS, bigDecimalPeriodFrequencyTypePair.getRight());
        }
        {
            JsonObject element = new JsonObject();
            element.addProperty("interestRatePerPeriod", 0.6);
            element.addProperty("interestRateFrequencyType", 0);//1 for months
            element.addProperty("locale", "en_US");

            System.out.println(Locale.getDefault());

            Pair<BigDecimal, PeriodFrequencyType> bigDecimalPeriodFrequencyTypePair = assembler.calculateAnnualInterest(element);

            assertEquals(bigDecimalPeriodFrequencyTypePair.getLeft(), new BigDecimal("219.0"));
            assertEquals(PeriodFrequencyType.DAYS, bigDecimalPeriodFrequencyTypePair.getRight());
        }

    }
}