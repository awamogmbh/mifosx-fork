package org.mifosplatform.portfolio.loanaccount.loanschedule.service;

import com.google.gson.JsonObject;
import org.junit.Assert;
import org.junit.Test;
import org.mifosplatform.infrastructure.core.api.JsonQuery;
import org.mifosplatform.infrastructure.core.serialization.FromJsonHelper;
import org.mifosplatform.portfolio.common.domain.PeriodFrequencyType;
import org.mifosplatform.portfolio.loanproduct.domain.LoanProduct;
import org.mifosplatform.portfolio.loanproduct.domain.LoanProductRelatedDetail;

import static org.junit.Assert.*;

public class LoanScheduleCalculationPlatformServiceImplTest {

    @Test
    public void setDefaultInterestPeriodFrequencyType() {


        {
            FromJsonHelper fromApiJsonHelper = new FromJsonHelper();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("interestRateFrequencyType", 99);
            jsonObject.addProperty("locale", "en_US");
            LoanProductRelatedDetail loanProductRelatedDetail = new LoanProductRelatedDetail();

            loanProductRelatedDetail.setRepaymentPeriodFrequencyType(PeriodFrequencyType.MONTHS);
            LoanProduct product = new LoanProduct(loanProductRelatedDetail);


            JsonQuery query = JsonQuery.from("", jsonObject, fromApiJsonHelper);


            LoanScheduleCalculationPlatformServiceImpl
                    .setDefaultInterestPeriodFrequencyType(query, product);

            Assert.assertEquals(99, query.longValueOfParameterNamed("interestRateFrequencyType").longValue());


        }

        {
            //do not set type on json object
            FromJsonHelper fromApiJsonHelper = new FromJsonHelper();

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("locale", "en_US");
            LoanProductRelatedDetail loanProductRelatedDetail = new LoanProductRelatedDetail();

            loanProductRelatedDetail.updatenterestPeriodFrequencyType(PeriodFrequencyType.DAYS);
            LoanProduct product = new LoanProduct(loanProductRelatedDetail);


            JsonQuery query = JsonQuery.from("", jsonObject, fromApiJsonHelper);


            LoanScheduleCalculationPlatformServiceImpl
                    .setDefaultInterestPeriodFrequencyType(query, product);

            Assert.assertEquals(PeriodFrequencyType.DAYS.getValue().longValue(), query.longValueOfParameterNamed("interestRateFrequencyType").longValue());


        }

        {
            //put null on product
            FromJsonHelper fromApiJsonHelper = new FromJsonHelper();

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("locale", "en_US");
            LoanProductRelatedDetail loanProductRelatedDetail = new LoanProductRelatedDetail();

            loanProductRelatedDetail.updatenterestPeriodFrequencyType(null);
            LoanProduct product = new LoanProduct(loanProductRelatedDetail);


            JsonQuery query = JsonQuery.from("", jsonObject, fromApiJsonHelper);


            LoanScheduleCalculationPlatformServiceImpl
                    .setDefaultInterestPeriodFrequencyType(query, product);

            Assert.assertEquals(PeriodFrequencyType.INVALID.getValue().longValue(), query.longValueOfParameterNamed("interestRateFrequencyType").longValue());


        }
    }
}