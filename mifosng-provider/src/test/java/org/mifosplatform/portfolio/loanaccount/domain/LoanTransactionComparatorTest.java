package org.mifosplatform.portfolio.loanaccount.domain;

import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mifosplatform.organisation.monetary.domain.MonetaryCurrency;
import org.mifosplatform.organisation.monetary.domain.Money;
import org.mifosplatform.organisation.monetary.domain.MoneyHelper;
import org.mifosplatform.organisation.office.domain.Office;
import org.mifosplatform.useradministration.domain.AppUser;
import org.springframework.data.jpa.domain.AbstractPersistable;

import java.lang.reflect.Field;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.Arrays.asList;
import static org.apache.commons.lang.StringUtils.rightPad;
import static org.mifosplatform.portfolio.loanaccount.domain.LoanTransactionType.*;

public class LoanTransactionComparatorTest {

    AtomicInteger counter;
    Loan loan;
    Office office;
    AppUser appUser;


    @Before
    public void beforeEach() {
        loan = new Loan();
        office = Office.headOffice("head", LocalDate.now(), "XXXX");
        appUser = new AppUser() {
        };

        MoneyHelper.overrideGlobalRoundingMode(RoundingMode.HALF_EVEN);
    }

    @Test
    public void testComparingTransactions() throws NoSuchFieldException, IllegalAccessException {
        // different dates
        //same date different time
        //same time different tx types
        //same time same transactions

        LocalDate startTxDate = LocalDate.now();
        LocalDateTime startTxTime = LocalDateTime.now();


        List<LoanTransaction> loanTransactions = asList(
                createTx(1, REPAYMENT, startTxDate, startTxTime),
                createTx(2, REPAYMENT, startTxDate.plusDays(2), startTxTime.plusDays(1)),
                createTx(3, WAIVE_CHARGES, startTxDate.plusDays(3), startTxTime.plusDays(1)),
                createTx(4, WAIVE_INTEREST, startTxDate.plusDays(4), startTxTime.plusDays(4)),
                createTx(5, CHARGE_PAYMENT, startTxDate.plusDays(5), startTxTime.plusDays(5)),

                //same day transactions different time
                createTx(6, REPAYMENT, startTxDate.plusDays(5), startTxTime.plusDays(5).plusMinutes(1)),
                createTx(7, REPAYMENT, startTxDate.plusDays(5), startTxTime.plusDays(5).plusMinutes(2)),
                createTx(8, WAIVE_CHARGES, startTxDate.plusDays(5), startTxTime.plusDays(5).plusMinutes(3)),
                createTx(9, WAIVE_INTEREST, startTxDate.plusDays(5), startTxTime.plusDays(5).plusMinutes(4)),

                //same day , same time, different tx types
                createTx(13, WAIVE_CHARGES, startTxDate.plusDays(6), startTxTime.plusDays(6)),
                createTx(14, WAIVE_INTEREST, startTxDate.plusDays(6), startTxTime.plusDays(6)),
                createTx(10, REPAYMENT, startTxDate.plusDays(6), startTxTime.plusDays(6)),
                createTx(11, REPAYMENT, startTxDate.plusDays(6), startTxTime.plusDays(6))

        );



        List<LoanTransaction> copy = multiShuffle(loanTransactions);
        printTransactions(copy, "Shuffled...");


        LoanTransactionComparator comparator = new LoanTransactionComparator();
        Collections.sort(copy, comparator);
        printTransactions(copy, "After Sort....");

        String join = toIdString(copy);
        Assert.assertEquals("1,2,3,4,5,6,7,8,9,14,13,10,11", join);

    }


    @Test
    public void testNullComparisonsOnIds() throws NoSuchFieldException, IllegalAccessException {
        LocalDate startTxDate = LocalDate.now();
        LocalDateTime startTxTime = LocalDateTime.now();
        List<LoanTransaction> loanTransactions =
                asList(
                        //same day , same time, different tx types
                        createTx(null, WAIVE_INTEREST, startTxDate.plusDays(6), startTxTime.plusDays(6)),
                        createTx(null, REPAYMENT, startTxDate.plusDays(6), startTxTime.plusDays(6)),
                        createTx(14, WAIVE_INTEREST, startTxDate.plusDays(6), startTxTime.plusDays(6)),
                        createTx(15, WAIVE_INTEREST, startTxDate.plusDays(6), startTxTime.plusDays(6)),
                        createTx(13, WAIVE_CHARGES, startTxDate.plusDays(6), startTxTime.plusDays(6)),
                        createTx(10, REPAYMENT, startTxDate.plusDays(6), startTxTime.plusDays(6))
                );


        List<LoanTransaction> copy = multiShuffle(loanTransactions);
        printTransactions(copy,"Shuffled");
        Collections.sort(copy,new LoanTransactionComparator());

        String join = toIdString(copy);
        Assert.assertEquals("14,15,null,13,10,null", join);
        Assert.assertEquals(copy.get(copy.size()-1).getTypeOf(),REPAYMENT);
        Assert.assertEquals(copy.get(2).getTypeOf(),WAIVE_INTEREST);

    }

    private String toIdString(List<LoanTransaction> loanTransactions) {
        List<String> ints = new ArrayList<>();
        for (LoanTransaction tx : loanTransactions) {
            ints.add(Objects.toString(tx.getId()));
        }
        return StringUtils.join(ints, ",");
    }


    private void printTransactions(List<LoanTransaction> transactions, String s) {
        System.out.println(s);
        System.out.printf("%s %s %s %s%n",
                rightPad("ID", 10),
                rightPad("TYPE", 15),
                rightPad("DATE", 12),
                rightPad("TIME", 10));

        for (LoanTransaction tx : transactions) {
            System.out.printf("%s %s %s %s%n",
                    rightPad(tx.getId() + "", 10),
                    rightPad(tx.getTypeOf().toString(), 15),
                    rightPad(tx.getTransactionDate().toString(), 12),
                    rightPad(tx.getCreatedDateTime().toString(), 10));
        }
    }


    private LoanTransaction createTx(Integer id, LoanTransactionType type, LocalDate txDate, LocalDateTime creationTime) throws NoSuchFieldException, IllegalAccessException {
        LoanTransaction loanTransaction = createTx(type, txDate, creationTime);
        Field id1 = AbstractPersistable.class.getDeclaredField("id");
        id1.setAccessible(true);
        id1.set(loanTransaction, id);
        return loanTransaction;
    }

    private LoanTransaction createTx(LoanTransactionType type, LocalDate txDate, LocalDateTime creationTime) {
        switch (type) {
            case WAIVE_CHARGES:
                return LoanTransaction.waiveLoanCharge(loan, office, zero(), txDate, zero(), zero(), zero(), creationTime, appUser);
            case WAIVE_INTEREST:
                return LoanTransaction.waiver(office, loan, zero(), txDate, zero(), zero(), creationTime, appUser);
            default:
                return LoanTransaction.repayment(office, zero(), null, txDate, "XXXX", creationTime, appUser);
        }
    }

    private List<LoanTransaction> multiShuffle(List<LoanTransaction> transactions) {
        List<LoanTransaction> copy = new ArrayList<>(transactions);
        for (int i = 0; i < 10; i++) {
            Collections.shuffle(copy);
        }
        return copy;
    }


    private static Money zero() {
        return Money.zero(new MonetaryCurrency("UGX", 0, 0));
    }

}