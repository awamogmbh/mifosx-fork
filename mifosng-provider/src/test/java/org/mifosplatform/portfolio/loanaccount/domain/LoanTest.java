package org.mifosplatform.portfolio.loanaccount.domain;

import com.google.gson.JsonObject;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.mifosplatform.infrastructure.core.api.JsonCommand;
import org.mifosplatform.infrastructure.core.serialization.FromJsonHelper;
import org.mifosplatform.organisation.monetary.domain.MonetaryCurrency;
import org.mifosplatform.organisation.monetary.domain.Money;
import org.mifosplatform.organisation.monetary.domain.MoneyHelper;
import org.mifosplatform.organisation.office.domain.Office;
import org.mifosplatform.portfolio.accountdetails.domain.AccountType;
import org.mifosplatform.portfolio.client.domain.Client;
import org.mifosplatform.portfolio.group.domain.Group;
import org.mifosplatform.portfolio.loanaccount.guarantor.service.GuarantorDomainServiceImpl;
import org.mifosplatform.portfolio.loanproduct.domain.LoanProductRelatedDetail;
import org.mifosplatform.portfolio.savings.domain.VslaAccount;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;


public class LoanTest {

    @Before
    public void beforeEach() {
        MoneyHelper.overrideGlobalRoundingMode(RoundingMode.HALF_EVEN);
    }

    @Test
    public void isOverPaid() {
        Loan l = new Loan();

        MoneyHelper.overrideGlobalRoundingMode(RoundingMode.HALF_EVEN);
        MonetaryCurrency currency = new MonetaryCurrency("UGX", 2, 1);

        final Money zero = Money.zero(currency);

        assertFalse(l.determineIfOverpaid(zero, zero));
        assertFalse(l.determineIfOverpaid(Money.of(currency, BigDecimal.ONE), Money.of(currency, BigDecimal.ONE)));
        assertFalse(l.determineIfOverpaid(Money.of(currency, BigDecimal.ONE).negated(), Money.of(currency, BigDecimal.ONE)));
        assertFalse(l.determineIfOverpaid(Money.of(currency, BigDecimal.ONE).negated(), zero));

        assertTrue(l.determineIfOverpaid(Money.of(currency, BigDecimal.ONE), Money.of(currency, BigDecimal.ONE).negated()));
        assertTrue(l.determineIfOverpaid(Money.of(currency, BigDecimal.ONE), zero));
    }

    @Test
    public void testRetrieveListOfTransactionsExcludeAccrualsAndTransfer() throws Exception {
        Loan loan = new Loan();

        LoanTransaction repaymentTransaction = loanTransactionOfType(LoanTransactionType.REPAYMENT);
        LoanTransaction waiveInterestTransaction = loanTransactionOfType(LoanTransactionType.WAIVE_INTEREST);
        LoanTransaction disbursementTransaction = loanTransactionOfType(LoanTransactionType.DISBURSEMENT);
        LoanTransaction initiateTransferTransaction = loanTransactionOfType(LoanTransactionType.INITIATE_TRANSFER);
        LoanTransaction approveTransferTransaction = loanTransactionOfType(LoanTransactionType.APPROVE_TRANSFER);
        LoanTransaction accrualTransaction = loanTransactionOfType(LoanTransactionType.ACCRUAL);

        List<LoanTransaction> loanTransactions = loan.getLoanTransactions();
        loanTransactions.add(repaymentTransaction);
        loanTransactions.add(disbursementTransaction);
        loanTransactions.add(waiveInterestTransaction);
        loanTransactions.add(initiateTransferTransaction);
        loanTransactions.add(approveTransferTransaction);
        loanTransactions.add(accrualTransaction);

        List<LoanTransaction> nonAccrualAndNonTransferLoanTransactions = loan.retrieveListOfTransactionsExcludeAccrualsAndTransfer();

        assertEquals(3, nonAccrualAndNonTransferLoanTransactions.size());
        assertTrue(nonAccrualAndNonTransferLoanTransactions.contains(repaymentTransaction));
        assertTrue(nonAccrualAndNonTransferLoanTransactions.contains(waiveInterestTransaction));
        assertTrue(nonAccrualAndNonTransferLoanTransactions.contains(disbursementTransaction));
        assertFalse(nonAccrualAndNonTransferLoanTransactions.contains(initiateTransferTransaction));
        assertFalse(nonAccrualAndNonTransferLoanTransactions.contains(approveTransferTransaction));
        assertFalse(nonAccrualAndNonTransferLoanTransactions.contains(accrualTransaction));
    }

    private LoanTransaction loanTransactionOfType(LoanTransactionType loanTransactionType) throws ReflectiveOperationException {
        LoanTransaction loanTransaction = new LoanTransaction();
        Field field = LoanTransaction.class.getDeclaredField("typeOf");
        field.setAccessible(true);
        Field modifiers = Field.class.getDeclaredField("modifiers");
        modifiers.setAccessible(true);
        modifiers.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        field.set(loanTransaction, loanTransactionType.getValue());
        return loanTransaction;
    }

    @Test
    public void testComputeGuarantorAmount_ShouldReturnZeroForZeroTotalGuaranteeAmount() {
        MoneyHelper.overrideGlobalRoundingMode(RoundingMode.HALF_EVEN);
        BigDecimal amountRemaining = BigDecimal.valueOf(10); // Amount unreleased from a specific guarantor funding
        BigDecimal amountForRelease = BigDecimal.valueOf(100); // Amount available for release from the loan transaction
        BigDecimal totalGuaranteeAmount = BigDecimal.valueOf(200); // Total of all the remaining amounts
        BigDecimal guarantorAmount = GuarantorDomainServiceImpl.computeGuarantorAmount(amountRemaining, amountForRelease, totalGuaranteeAmount);

        assertEquals(BigDecimal.valueOf(10 * 100 / 200), guarantorAmount);
        totalGuaranteeAmount = BigDecimal.ZERO; // No money is available to release the guarantor's funding
        guarantorAmount = GuarantorDomainServiceImpl.computeGuarantorAmount(amountRemaining, amountForRelease, totalGuaranteeAmount);
        assertEquals(BigDecimal.ZERO, guarantorAmount);
    }

    @Test
    public void testGetLoanOffices() {
        final Office clientOffice = new Office(11L, LocalDate.now());
        final Office groupOffice = new Office(12L, LocalDate.now());
        final Office subVslaOffice = new Office(13L, LocalDate.now());
        final Office vslaMemberOffice = new Office(14L, LocalDate.now());

        JsonObject parsedCommand = new JsonObject();
        parsedCommand.addProperty("lastname", "lastname");
        parsedCommand.addProperty("firstname", "firstname");
        final JsonCommand jsonCommand = JsonCommand.fromExistingCommand(11L, null, parsedCommand, new FromJsonHelper(), null, null, null, null, null, null);
        final String accountNo = "00001";
        final Client client = Client.createNew(null, clientOffice, null, null, null, null, null, null, jsonCommand);
        client.setId(11L);
        final Set<Client> clientMembers = new HashSet<>();
        final Set<Group> groupMembers = new HashSet<>();
        final Group group = Group.newGroup(groupOffice, null, null, null, null, null, true, LocalDate.now(), clientMembers, groupMembers, LocalDate.now(), null, null);
        final VslaAccount subVsla = new VslaAccount() {{
            setOfficeId(subVslaOffice);
        }};
        final VslaAccount vslaMember = new VslaAccount() {{
            setOfficeId(vslaMemberOffice);
        }};
        final Integer individualLoanType = AccountType.INDIVIDUAL.getValue();
        final Integer groupLoanType = AccountType.GROUP.getValue();
        final Integer subVslaLoanType = AccountType.SUB_VSLA.getValue();
        final Integer vslaMemberLoanType = AccountType.VSLA_MEMBER.getValue();

        final LoanProductRelatedDetail loanRepaymentScheduleDetail = new LoanProductRelatedDetail();
        loanRepaymentScheduleDetail.updateNumberOfRepayments(10);
        loanRepaymentScheduleDetail.setPrincipal(BigDecimal.valueOf(1200000));
        loanRepaymentScheduleDetail.updateCurrency(new MonetaryCurrency("UGX", 2, 100));
        Loan individualLoan = Loan.newIndividualLoanApplication(accountNo, client, individualLoanType, null,
                null, null, null,
                null, loanRepaymentScheduleDetail, null,
                null, null, null,
                null, null,
                null, null);
        Loan groupLoan = Loan.newGroupLoanApplication(accountNo, group, groupLoanType, null,
                null, null, null,
                null, loanRepaymentScheduleDetail, null,
                null, null, null,
                null, null, null, null, null);
        Loan subVslaLoan = Loan.newVslaLoanApplication(accountNo, subVsla, subVslaLoanType, null,
                null, null, null,
                null, loanRepaymentScheduleDetail, null,
                null, null, null,
                null, null, null, null, null);
        Loan vslaMemberLoan = Loan.newIndividualLoanApplicationFromVsla(accountNo, client, vslaMember, vslaMemberLoanType, null,
                null, null, null,
                null, loanRepaymentScheduleDetail, null,
                null, null, null,
                null, null, null, null, null);


        assertEquals(11L, individualLoan.getOffice().getId().longValue());
        assertEquals(12L, groupLoan.getOffice().getId().longValue());
        assertEquals(13L, subVslaLoan.getOffice().getId().longValue());
        assertEquals(14L, vslaMemberLoan.getOffice().getId().longValue());

        assertEquals(11L, individualLoan.getOfficeId().longValue());
        assertEquals(12L, groupLoan.getOfficeId().longValue());
        assertEquals(13L, subVslaLoan.getOfficeId().longValue());
        assertEquals(14L, vslaMemberLoan.getOfficeId().longValue());
    }

    @Test
    public void testCreateSubVsla_vslaMemberLoan() {
        final Office clientOffice = new Office(11L, LocalDate.now());
        final Office subVslaOffice = new Office(13L, LocalDate.now());
        final Office vslaMemberOffice = new Office(14L, LocalDate.now());
        JsonObject parsedCommand = new JsonObject();
        parsedCommand.addProperty("lastname", "lastname");
        parsedCommand.addProperty("firstname", "firstname");
        final JsonCommand jsonCommand = JsonCommand.fromExistingCommand(11L, null, parsedCommand, new FromJsonHelper(), null, null, null, null, null, null);
        final String accountNo = "00001";
        final Client client = Client.createNew(null, clientOffice, null, null, null, null, null, null, jsonCommand);
        client.setId(11L);

        final VslaAccount subVsla = new VslaAccount() {{
            setOfficeId(subVslaOffice);
        }};
        final VslaAccount vslaMember = new VslaAccount() {{
            setOfficeId(vslaMemberOffice);
        }};
        final Integer subVslaLoanType = AccountType.SUB_VSLA.getValue();
        final Integer vslaMemberLoanType = AccountType.VSLA_MEMBER.getValue();

        final LoanProductRelatedDetail loanRepaymentScheduleDetail = new LoanProductRelatedDetail();
        loanRepaymentScheduleDetail.updateNumberOfRepayments(10);
        loanRepaymentScheduleDetail.setPrincipal(BigDecimal.valueOf(1200000));
        loanRepaymentScheduleDetail.updateCurrency(new MonetaryCurrency("UGX", 2, 100));
        Loan subVslaLoan = Loan.newVslaLoanApplication(accountNo, subVsla, subVslaLoanType, null,
                null, null, null,
                null, loanRepaymentScheduleDetail, null,
                null, null, null,
                null, null, null, null, null);

        Loan vslaMemberLoan = Loan.newIndividualLoanApplicationFromVsla(accountNo, client, vslaMember, vslaMemberLoanType, null,
                null, null, null,
                null, loanRepaymentScheduleDetail, null,
                null, null, null,
                null, null, null, null, null);

        assertNull(vslaMemberLoan.client());
        assertNotNull(vslaMemberLoan.getVslaMemberId());
        assertEquals(Long.valueOf(11L), vslaMemberLoan.getVslaMemberId());
        assertNotNull(vslaMemberLoan.getVsla());

        assertNull(subVslaLoan.client());
        assertNull(subVslaLoan.getVslaMemberId());
        assertNotNull(subVslaLoan.getVsla());
    }
}