package org.mifosplatform.portfolio.charge.domain;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ChargeCalculationTypeTest {

    @Test
    public void isUnknown() {
        //awamo type are unknown to mifos
        assertTrue(ChargeCalculationType.fromInt(6).isUnknown());//PERCENT_TOTAL_OUTSTANDING_PRINCIPAL
        assertTrue(ChargeCalculationType.fromInt(0).isUnknown());//PERCENT_TOTAL_OUTSTANDING_PRINCIPAL
        assertTrue(ChargeCalculationType.fromInt(7).isUnknown());//PERCENT_TOTAL_OUTSTANDING_PRINCIPAL_INTEREST_FEES
        assertTrue(ChargeCalculationType.fromInt(8).isUnknown());//PERCENT_TOTAL_OUTSTANDING_PRINCIPAL_INTEREST

        assertFalse(ChargeCalculationType.fromInt(1).isUnknown());
        assertFalse(ChargeCalculationType.fromInt(2).isUnknown());
        assertFalse(ChargeCalculationType.fromInt(3).isUnknown());
        assertFalse(ChargeCalculationType.fromInt(4).isUnknown());
        assertFalse(ChargeCalculationType.fromInt(5).isUnknown());
    }
}