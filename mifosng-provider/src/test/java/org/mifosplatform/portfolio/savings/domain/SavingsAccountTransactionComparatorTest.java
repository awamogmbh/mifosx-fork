package org.mifosplatform.portfolio.savings.domain;

import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mifosplatform.organisation.monetary.domain.MonetaryCurrency;
import org.mifosplatform.organisation.monetary.domain.Money;
import org.mifosplatform.organisation.monetary.domain.MoneyHelper;
import org.mifosplatform.organisation.office.domain.Office;
import org.mifosplatform.portfolio.savings.SavingsAccountTransactionType;
import org.mifosplatform.useradministration.domain.AppUser;
import org.springframework.data.jpa.domain.AbstractPersistable;

import java.lang.reflect.Field;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.Arrays.asList;
import static org.apache.commons.lang.StringUtils.rightPad;

public class SavingsAccountTransactionComparatorTest {
    AtomicInteger counter;
    SavingsAccount loan;
    Office office;
    AppUser appUser;
    List<SavingsAccountTransaction> transactions;


    @Before
    public void beforeEach() {
        loan = new SavingsAccount();
        office = Office.headOffice("head", LocalDate.now(), "XXXX");
        appUser = new AppUser() {
        };

        MoneyHelper.overrideGlobalRoundingMode(RoundingMode.HALF_EVEN);
    }

    @Test
    public void testComparingTransactions() throws NoSuchFieldException, IllegalAccessException {
        // different dates
        //same date different time
        //same time different tx types
        //same time same transactions

        LocalDate startTxDate = LocalDate.now();
        LocalDateTime startTxTime = LocalDateTime.now();


        List<SavingsAccountTransaction> savingsAccountTransactions = asList(
                createTx(1, SavingsAccountTransactionType.DEPOSIT, startTxDate, startTxTime),
                createTx(2, SavingsAccountTransactionType.DEPOSIT, startTxDate.plusDays(2), startTxTime.plusDays(1)),
                createTx(3, SavingsAccountTransactionType.WAIVE_CHARGES, startTxDate.plusDays(3), startTxTime.plusDays(1)),
                createTx(4, SavingsAccountTransactionType.WAIVE_CHARGES, startTxDate.plusDays(4), startTxTime.plusDays(4)),
                createTx(5, SavingsAccountTransactionType.PAY_CHARGE, startTxDate.plusDays(5), startTxTime.plusDays(5)),

                //same day transactions different time
                createTx(6, SavingsAccountTransactionType.DEPOSIT, startTxDate.plusDays(5), startTxTime.plusDays(5).plusMinutes(1)),
                createTx(7, SavingsAccountTransactionType.DEPOSIT, startTxDate.plusDays(5), startTxTime.plusDays(5).plusMinutes(2)),
                createTx(8, SavingsAccountTransactionType.WAIVE_CHARGES, startTxDate.plusDays(5), startTxTime.plusDays(5).plusMinutes(3)),
                createTx(9, SavingsAccountTransactionType.WAIVE_CHARGES, startTxDate.plusDays(5), startTxTime.plusDays(5).plusMinutes(4)),

                //same day , same time, different tx types, but different ids
                createTx(13, SavingsAccountTransactionType.WAIVE_CHARGES, startTxDate.plusDays(6), startTxTime.plusDays(6)),
                createTx(14, SavingsAccountTransactionType.WAIVE_CHARGES, startTxDate.plusDays(6), startTxTime.plusDays(6)),
                createTx(10, SavingsAccountTransactionType.WITHDRAWAL, startTxDate.plusDays(6), startTxTime.plusDays(6)),
                createTx(11, SavingsAccountTransactionType.DEPOSIT, startTxDate.plusDays(6), startTxTime.plusDays(6))
        );


        List<SavingsAccountTransaction> copy = multiShuffle(savingsAccountTransactions);
        printTransactions(copy, "Shuffled...");


        SavingsAccountTransactionComparator comparator = new SavingsAccountTransactionComparator();
        Collections.sort(copy, comparator);
        printTransactions(copy, "After Sort....");

        String join = toIdString(copy);
        Assert.assertEquals("1,2,3,4,5,6,7,8,9,10,11,13,14", join);

    }


    @Test
    public void testNullComparisonsOnIds() throws NoSuchFieldException, IllegalAccessException {
        LocalDate startTxDate = LocalDate.now();
        LocalDateTime startTxTime = LocalDateTime.now();
        List<SavingsAccountTransaction> savingsAccountTransactions =
                asList(
                        //same day , same time, different tx types
                        createTx(null, SavingsAccountTransactionType.WAIVE_CHARGES, startTxDate.plusDays(6), startTxTime.plusDays(6).plusSeconds(1)),
                        createTx(null, SavingsAccountTransactionType.DEPOSIT, startTxDate.plusDays(6), startTxTime.plusDays(6)),
                        createTx(14, SavingsAccountTransactionType.WAIVE_CHARGES, startTxDate.plusDays(6), startTxTime.plusDays(6)),
                        createTx(15, SavingsAccountTransactionType.WITHDRAWAL, startTxDate.plusDays(6), startTxTime.plusDays(6)),
                        createTx(13, SavingsAccountTransactionType.WITHDRAWAL, startTxDate.plusDays(6), startTxTime.plusDays(6)),
                        createTx(10, SavingsAccountTransactionType.PAY_CHARGE, startTxDate.plusDays(6), startTxTime.plusDays(6))
                );


        List<SavingsAccountTransaction> copy = multiShuffle(savingsAccountTransactions);
        printTransactions(copy, "Shuffled");
        Collections.sort(copy, new SavingsAccountTransactionComparator());
        printTransactions(copy,"Sorted...");

        String join = toIdString(copy);
        Assert.assertEquals("10,13,14,15,null,null", join);
        Assert.assertEquals(SavingsAccountTransactionType.WAIVE_CHARGES,copy.get(copy.size() - 1).typeOf());
        Assert.assertEquals(SavingsAccountTransactionType.WAIVE_CHARGES, copy.get(2).typeOf());

    }

    private String toIdString(List<SavingsAccountTransaction> loanTransactions) {
        List<String> ints = new ArrayList<>();
        for (SavingsAccountTransaction tx : loanTransactions) {
            ints.add(Objects.toString(tx.getId()));
        }
        return StringUtils.join(ints, ",");
    }


    private void printTransactions(List<SavingsAccountTransaction> transactions, String s) {
        System.out.println(s);
        System.out.printf("%s %s %s %s%n",
                rightPad("ID", 10),
                rightPad("TYPE", 15),
                rightPad("DATE", 12),
                rightPad("TIME", 10));

        for (SavingsAccountTransaction tx : transactions) {
            System.out.printf("%s %s %s %s%n",
                    rightPad(tx.getId() + "", 10),
                    rightPad(tx.typeOf().toString(), 15),
                    rightPad(tx.transactionLocalDate().toString(), 12),
                    rightPad(tx.createdDate().toString(), 10));
        }
    }


    private SavingsAccountTransaction createTx(Integer id, SavingsAccountTransactionType type, LocalDate txDate, LocalDateTime creationTime) throws NoSuchFieldException, IllegalAccessException {
        SavingsAccountTransaction loanTransaction = createTx(type, txDate, creationTime);
        Field id1 = AbstractPersistable.class.getDeclaredField("id");
        id1.setAccessible(true);
        id1.set(loanTransaction, id);
        return loanTransaction;
    }

    private SavingsAccountTransaction createTx(SavingsAccountTransactionType type, LocalDate txDate, LocalDateTime creationTime) {
        switch (type) {
            case WAIVE_CHARGES:
                return SavingsAccountTransaction.waiver(loan, office, txDate, creationTime, zero(), appUser);
            case INTEREST_POSTING:
                return SavingsAccountTransaction.interestPosting(loan, office, null, txDate, creationTime.toDate(TimeZone.getTimeZone("UTC")), zero());
            case WITHDRAWAL:
                return SavingsAccountTransaction.withdrawal(loan, office, null, txDate, zero(), creationTime.toDate(), appUser);
            case DEPOSIT:
                return SavingsAccountTransaction.deposit(loan, office, null, txDate, zero(), creationTime.toDate(), appUser);
            case PAY_CHARGE:
                return SavingsAccountTransaction.charge(loan, office, txDate, creationTime, zero(), appUser);
            default:
                throw new RuntimeException("unsupported transaction type: [" + type + "]");
        }
    }

    private <T> List<T> multiShuffle(List<T> transactions) {
        List<T> copy = new ArrayList<>(transactions);
        for (int i = 0; i < 10; i++) {
            Collections.shuffle(copy);
        }
        return copy;
    }


    private static Money zero() {
        return Money.zero(new MonetaryCurrency("UGX", 0, 0));
    }

}