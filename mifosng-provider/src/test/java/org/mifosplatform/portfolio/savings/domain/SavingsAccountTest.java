package org.mifosplatform.portfolio.savings.domain;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class SavingsAccountTest {

    @Test
    public void testGetUnreversedSavingsAccountTransactions(){
        SavingsAccount savingsAccount = new SavingsAccount();

        List<SavingsAccountTransaction> savingsAccountTransactions = new ArrayList<>();
        SavingsAccountTransaction savingsAccountTransaction1 = new SavingsAccountTransaction();
        SavingsAccountTransaction savingsAccountTransaction2 = new SavingsAccountTransaction();
        SavingsAccountTransaction savingsAccountTransaction3 = new SavingsAccountTransaction();
        SavingsAccountTransaction savingsAccountTransaction4 = new SavingsAccountTransaction();
        SavingsAccountTransaction savingsAccountTransaction5 = new SavingsAccountTransaction();
        SavingsAccountTransaction savingsAccountTransaction6 = new SavingsAccountTransaction();
        SavingsAccountTransaction savingsAccountTransaction7 = new SavingsAccountTransaction();
        savingsAccountTransaction7.reverse();

        savingsAccountTransactions.add(savingsAccountTransaction1);
        savingsAccountTransactions.add(savingsAccountTransaction2);
        savingsAccountTransactions.add(savingsAccountTransaction3);
        savingsAccountTransactions.add(savingsAccountTransaction4);
        savingsAccountTransactions.add(savingsAccountTransaction5);
        savingsAccountTransactions.add(savingsAccountTransaction6);
        savingsAccountTransactions.add(savingsAccountTransaction7);

        List<SavingsAccountTransaction> unreversedSavingsAccountTransactions = savingsAccount.getUnreversedSavingsAccountTransactions(savingsAccountTransactions);
        assertFalse(unreversedSavingsAccountTransactions.isEmpty());
        assertEquals(6, unreversedSavingsAccountTransactions.size());
        for(SavingsAccountTransaction savingsAccountTransaction : unreversedSavingsAccountTransactions){
            assertFalse(savingsAccountTransaction.isReversed());
        }
    }
    
}
