package org.mifosplatform.portfolio.savings.domain;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class SavingsAccountTransactionTest {

    @Test
    public void testCopyTransaction_ShouldAlsoCopyChargesPaidBy(){
        SavingsAccountTransaction transaction = new SavingsAccountTransaction();
        SavingsAccountCharge savingsAccountCharge = new SavingsAccountCharge();
        SavingsAccountChargePaidBy chargePaidBy = SavingsAccountChargePaidBy.instance(transaction, savingsAccountCharge, BigDecimal.valueOf(10000));
        transaction.getSavingsAccountChargesPaid().add(chargePaidBy);

        SavingsAccountTransaction accountTransaction = SavingsAccountTransaction.copyTransaction(transaction);
        assertEquals(1, accountTransaction.getSavingsAccountChargesPaid().size());
        for (SavingsAccountChargePaidBy savingsAccountChargePaidBy : accountTransaction.getSavingsAccountChargesPaid()){
            assertEquals(BigDecimal.valueOf(10000), savingsAccountChargePaidBy.getAmount());
            assertNotEquals(chargePaidBy, savingsAccountChargePaidBy);
        }
    }
}
