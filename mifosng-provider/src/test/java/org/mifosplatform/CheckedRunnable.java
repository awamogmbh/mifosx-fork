package org.mifosplatform;

public interface CheckedRunnable {

    void run() throws Throwable;

}
