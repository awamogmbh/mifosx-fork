package org.mifosplatform;

import static org.junit.Assert.fail;

public class TestUtils {


    //should run a CheckedRunnable and assert an exception
    public static <T extends Throwable> T shouldFail(final CheckedRunnable runnable, final Class<T> expectedException) {
        try {
            runnable.run();
            fail("Expected exception " + expectedException.getName());
        } catch (final Throwable actualException) {
            if (actualException.getClass().equals(expectedException)) {
                return (T) actualException;
            }
            throw new RuntimeException(String.format("Expected exception %s but got %s", expectedException, actualException.getClass()), actualException);
        }
        throw new RuntimeException("Expected an exception ");
    }

    public static <T extends Throwable> T shouldFail(final CheckedRunnable runnable) {
        try {
            runnable.run();
            fail("Expected an exception ");
        } catch (final Throwable actualException) {
            return (T) actualException;
        }
        throw new RuntimeException("Expected an exception ");
    }

    public static void shouldNotFail(final CheckedRunnable runnable) {
        try {
            runnable.run();
        } catch (final Throwable actualException) {
            throw new RuntimeException("Expected no exception but got " + actualException.getClass(), actualException);
        }
    }
}
