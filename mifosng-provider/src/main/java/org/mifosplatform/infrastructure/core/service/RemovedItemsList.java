package org.mifosplatform.infrastructure.core.service;

import java.util.*;
import java.util.concurrent.Callable;

public class RemovedItemsList<E> extends ArrayList<E> {


    private final transient List<E> removedItems = new ArrayList<>();
    private boolean initialized = false;

    public RemovedItemsList() {
        super();
    }

    public RemovedItemsList(int initialCapacity) {
        super(initialCapacity);
    }


    public RemovedItemsList(Collection<? extends E> c) {
        super(c);
        initialized = true;
    }

    @Override
    public boolean add(E e) {
        initialized = true;
        return super.add(e);
    }


    @Override
    public void add(int index, E element) {
        initialized = true;
        super.add(index, element);
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        initialized = true;
        return super.addAll(c);
    }


    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        initialized = true;
        return super.addAll(index, c);
    }

    @Override
    public boolean remove(Object o) {
        boolean removed = super.remove(o);
        if (removed) removedItems.add((E) o);
        return removed;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean removed = false;
        for (Object o : c) {
            boolean removedTmp = remove(o);
            removed = removed || removedTmp;
        }
        return removed;
    }

    @Override
    public E remove(int index) {
        E remove = super.remove(index);
        if (remove != null) removedItems.add(remove);
        return remove;
    }

    @Override
    public void clear() {
        removedItems.addAll(this);
        super.clear();
    }

    public List<E> getRemovedItems() {
        return removedItems;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public boolean isInitialized() {
        return initialized;
    }

    public RemovedItemsList<E> initIfNot(Callable<List<E>> callable) {
        if (initialized) return this;
        try {
            List<E> call = callable.call();
            if (call != null) addAll(call);
            initialized = true;
            return this;
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public Set<E> asSet() {
        return new HashSet<>(this);
    }
}
