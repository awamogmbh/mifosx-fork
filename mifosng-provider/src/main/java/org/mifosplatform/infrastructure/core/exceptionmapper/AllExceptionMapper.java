package org.mifosplatform.infrastructure.core.exceptionmapper;

import org.mifosplatform.infrastructure.core.data.ApiGlobalErrorResponse;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Component
@Scope("singleton")
public class AllExceptionMapper implements ExceptionMapper<Exception> {
    @Override
    public Response toResponse(Exception exception) {
        final ApiGlobalErrorResponse dataValidationErrorResponse = ApiGlobalErrorResponse.serverSideError(
                exception.getMessage(), exception.getMessage(), "Internal Server Error");

        return Response.status(Response.Status.BAD_REQUEST)
                       .entity(dataValidationErrorResponse)
                       .type(MediaType.APPLICATION_JSON).build();

    }
}
