package org.mifosplatform.infrastructure.core.api;

import org.mifosplatform.infrastructure.core.service.TenantDatabaseUpgradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.xml.ws.Holder;

@Path("/backoffice")
@Component
@Scope("singleton")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
public class BackofficeResource {

    @Autowired
    TenantDatabaseUpgradeService upgradeService;
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(BackofficeResource.class);


    @Path("migrate/{tenantId}")
    @POST
    public Holder<String> migrate(@PathParam("tenantId") final String tenantId) {
        try {
            this.upgradeService.migrateTenant(tenantId);
        } catch (Throwable x) {//NOSONAR
            logger.error("error migrating tenant", x);
            throw new IllegalStateException("error migrating tenant [" + tenantId + "]", x);
        }
        return new Holder<>("migration completed for tenant [" + tenantId + "]");
    }



}
