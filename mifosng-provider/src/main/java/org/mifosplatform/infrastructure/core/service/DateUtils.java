/**
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package org.mifosplatform.infrastructure.core.service;

import org.joda.time.*;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.mifosplatform.infrastructure.core.data.ApiParameterError;
import org.mifosplatform.infrastructure.core.domain.MifosPlatformTenant;
import org.mifosplatform.infrastructure.core.exception.PlatformApiDataValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class DateUtils {

    private DateUtils() {
    }

    public static final String DD_MMM_YYYY_HH_MM_SS_AA = "dd/MMM/yyyy hh:mm:ss aa";
    public static final String DD_MMM_YYYY_HH_MM_SS_AA_UNDERSCORE = "dd-MMM-yyyy_hh-mm-ss-aa";
    private static final Logger LOG = LoggerFactory.getLogger(DateUtils.class);

    public static DateTimeZone getDateTimeZoneOfTenant() {
        final MifosPlatformTenant tenant = ThreadLocalContextUtil.getTenant();
        DateTimeZone zone = null;
        if (tenant != null) {
            zone = DateTimeZone.forID(tenant.getTimezoneId());
            TimeZone.getTimeZone(tenant.getTimezoneId());
        }
        return zone;
    }

    public static TimeZone getTimeZoneOfTenant() {
        final MifosPlatformTenant tenant = ThreadLocalContextUtil.getTenant();
        TimeZone zone = null;
        if (tenant != null) {
            zone = TimeZone.getTimeZone(tenant.getTimezoneId());
        }
        return zone;
    }

    public static Date getDateOfTenant() {
        return getLocalDateOfTenant().toDateTimeAtStartOfDay().toDate();
    }

    //implement the time machine
    public static LocalDate getLocalDateOfTenant() {

        LocalDate today = now().toLocalDate();

        final DateTimeZone zone = getDateTimeZoneOfTenant();
        if (zone != null) {
            today = now().withZone(zone).toLocalDate();
        }

        return today;
    }

    public static LocalDateTime getLocalDateTimeOfTenant() {

        LocalDateTime today = now().toLocalDateTime();

        final DateTimeZone zone = getDateTimeZoneOfTenant();
        if (zone != null) {
            today = now().withZone(zone).toLocalDateTime();
        }

        return today;
    }

    public static LocalDate parseLocalDate(final String stringDate, final String pattern) {

        try {
            final DateTimeFormatter dateStringFormat = DateTimeFormat.forPattern(pattern);
            dateStringFormat.withZone(getDateTimeZoneOfTenant());
            final DateTime dateTime = dateStringFormat.parseDateTime(stringDate);
            return dateTime.toLocalDate();
        } catch (final IllegalArgumentException e) {
            final List<ApiParameterError> dataValidationErrors = new ArrayList<>();
            final ApiParameterError error = ApiParameterError.parameterError("validation.msg.invalid.date.pattern", "The parameter date ("
                    + stringDate + ") is invalid w.r.t. pattern " + pattern, "date", stringDate, pattern);
            dataValidationErrors.add(error);
            throw new PlatformApiDataValidationException("validation.msg.validation.errors.exist", "Validation errors exist.",
                    dataValidationErrors);
        }
    }

    public static String formatToSqlDate(final Date date) {
        return formatDate(date, "yyyy-MM-dd");
    }

    public static String formatReadableDate(final Date date) {
        return formatDateNoTimeZone(date, DD_MMM_YYYY_HH_MM_SS_AA);
    }

    public static String formatDate(Date date, String s) {
        return formatDate(date, s, true);
    }

    public static String formatDateNoTimeZone(Date date, String s) {
        return formatDate(date, s, false);
    }

    public static String formatDate(Date date, String s, boolean setTimeZone) {
        final DateFormat df = new SimpleDateFormat(s);
        if (setTimeZone)
            df.setTimeZone(getTimeZoneOfTenant());
        return df.format(date);
    }

    public static boolean isDateInTheFuture(final LocalDate localDate) {
        return localDate.isAfter(getLocalDateOfTenant());
    }


    public static LocalDate nowAsLocalDate() {
        return now().toLocalDate();
    }

    public static Date nowAsDate() {
        return now().toDate();
    }

    /**
     * Having one central  place where to create new date. In plan to support
     * time zones
     */
    public static DateTime now() {
        DateTime date = __realNewDate();
        Period adjustment = adjustment();
        return adjustment != null && isITest() ? date.plus(adjustment) : date;
    }

    public static DateTime __realNewDate() {//NOSONAR
        return DateTime.now(DateTimeZone.UTC);
    }


    public static void resetTime() {
        if (LOG.isInfoEnabled())
            LOG.info("\n============\nRESETTING TIME {}\n=================", formatReadableDate(__realNewDate().toDate()));
        tenantAdjustments.remove(tenantId());
    }

    public static void changSystemTime(String dateString) throws ParseException {
        final Date date = new SimpleDateFormat(DD_MMM_YYYY_HH_MM_SS_AA).parse(dateString);
        changSystemTime(date);
    }

    public static void changSystemTime(Date date) {
        if (isITest()) {
            final Period adjustment = new Period(__realNewDate().getMillis(), date.getTime());
            tenantAdjustments.put(tenantId(), adjustment);
            if (LOG.isInfoEnabled())
                LOG.info("\n============\nADJUSTING TIME TO {} : [{}] \n=================", formatReadableDate(date), adjustment);

        } else {
            throw new UnsupportedOperationException();
        }
    }

    private static boolean isITest() {
        return tenantId().startsWith("peIntegrationTests") || isOnJenkins();
    }

    private static String tenantId() {
        final MifosPlatformTenant tenant = ThreadLocalContextUtil.getTenant();
        return tenant != null ? tenant.getTenantIdentifier() : "";
    }

    private static final Map<String, Period> tenantAdjustments = new ConcurrentHashMap<>();


    private static Period adjustment() {
        return tenantAdjustments.get(tenantId());
    }

    public static boolean isOnJenkins() {//NOSONAR
        boolean emulateJenkins = Boolean.parseBoolean(getSysProperty("EMULATE_JENKINS", "FALSE"));
        String groovyHome = getSysProperty("user.dir", "");
        return groovyHome.startsWith("/var/lib/jenkins") || emulateJenkins;
    }


    public static String getSysProperty(String property, String defaultValue) {
        String syProperty = System.getProperty(property);
        if (syProperty == null) {
            syProperty = System.getenv(property);
        }
        if (syProperty == null) {
            syProperty = defaultValue;
        }
        return syProperty;
    }

}