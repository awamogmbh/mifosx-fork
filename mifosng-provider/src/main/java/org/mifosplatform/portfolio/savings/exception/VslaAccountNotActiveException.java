package org.mifosplatform.portfolio.savings.exception;

import org.mifosplatform.infrastructure.core.exception.AbstractPlatformDomainRuleException;

public class VslaAccountNotActiveException extends AbstractPlatformDomainRuleException {

    public VslaAccountNotActiveException(final Long vslaId) {
        super("error.msg.vsla.not.active.exception", "The VslaAccount with id `" + vslaId + "` is not active", vslaId);
    }
}
