package org.mifosplatform.portfolio.savings.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface VslaAccountRepository extends JpaRepository<VslaAccount, Long>, JpaSpecificationExecutor<VslaAccount> {

}
