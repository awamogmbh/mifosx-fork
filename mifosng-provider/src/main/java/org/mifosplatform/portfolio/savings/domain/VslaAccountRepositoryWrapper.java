package org.mifosplatform.portfolio.savings.domain;

import org.mifosplatform.portfolio.savings.exception.VslaAccountNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VslaAccountRepositoryWrapper {
    private final VslaAccountRepository repository;

    @Autowired
    public VslaAccountRepositoryWrapper(final VslaAccountRepository repository) {
        this.repository = repository;
    }

    public VslaAccount findOneWithNotFoundDetection(final Long id) {
        final VslaAccount entity = this.repository.findOne(id);
        if (entity == null) { throw new VslaAccountNotFoundException(id); }
        return entity;
    }
}
