/**
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package org.mifosplatform.portfolio.savings.domain;

import org.mifosplatform.portfolio.savings.DepositAccountType;
import org.mifosplatform.portfolio.savings.exception.SavingsAccountNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * Wrapper for {@link SavingsAccountRepository} that is responsible for checking
 * if {@link SavingsAccount} is returned when using <code>findOne</code>
 * repository method and throwing an appropriate not found exception.
 * </p>
 * 
 * <p>
 * This is to avoid need for checking and throwing in multiple areas of code
 * base where {@link SavingsAccountRepository} is required.
 * </p>
 */
@Service
public class SavingsAccountRepositoryWrapper {

    private final SavingsAccountRepository repository;
    private final SavingsAccountTransactionRepository accountTransactionRepository;

    @Autowired
    public SavingsAccountRepositoryWrapper(final SavingsAccountRepository repository, SavingsAccountTransactionRepository accountTransactionRepository) {
        this.repository = repository;
        this.accountTransactionRepository = accountTransactionRepository;
    }

    public SavingsAccount findOneWithNotFoundDetection(final Long savingsId) {
        final SavingsAccount account = this.repository.findOne(savingsId);
        if (account == null) { throw new SavingsAccountNotFoundException(savingsId); }
        return account;
    }

    public SavingsAccount findOneWithNotFoundDetection(final Long savingsId, final DepositAccountType depositAccountType) {
        final SavingsAccount account = this.repository.findByIdAndDepositAccountType(savingsId, depositAccountType.getValue());
        if (account == null) { throw new SavingsAccountNotFoundException(savingsId); }
        return account;
    }

    public void save(final SavingsAccount account) {
        this.repository.save(account);
        saveTransactions(account);
    }

    private void saveTransactions(final SavingsAccount account) {
        List<SavingsAccountTransaction> deletedTransactions = account.deletedTransactions();
        for (SavingsAccountTransaction t : deletedTransactions) {
            mayBeDelete(t);
        }
        List<SavingsAccountTransaction> loanTransactions = account.getTransactions();
        for (SavingsAccountTransaction t : loanTransactions) {
            this.accountTransactionRepository.save(t);
            if (t.isReversed()) flush();
        }
    }
    private void mayBeDelete(SavingsAccountTransaction t) {
        if (t.getId() != null) accountTransactionRepository.delete(t);
    }

    public void flush() {
        repository.flush();
    }

    public void delete(final SavingsAccount account) {
        this.repository.delete(account);
    }

    public void saveAndFlush(final SavingsAccount account) {
        this.save(account);
        this.flush();
    }
}