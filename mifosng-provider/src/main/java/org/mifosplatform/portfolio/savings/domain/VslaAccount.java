package org.mifosplatform.portfolio.savings.domain;

import org.mifosplatform.organisation.office.domain.Office;
import org.mifosplatform.organisation.staff.domain.Staff;
import org.mifosplatform.portfolio.client.domain.Client;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
@Entity
@Table(name = "m_vsla")
public class VslaAccount extends AbstractPersistable<Long> {
    @NotNull
    @Column(name = "name")
    private String name;
    @NotNull
    @Column(name = "awamo_id")
    private String awamoId;
    @NotNull
    @JoinColumn(name = "office_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Office officeId;
    @JoinColumn(name = "parent_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private VslaAccount parentVsla;
    @NotNull
    @JoinColumn(name = "responsible_loan_officer")
    @ManyToOne(fetch = FetchType.LAZY)
    private Staff responsibleLoanOfficer;
    @NotNull
    @JoinColumn(name = "vsla_representative")
    @ManyToOne(fetch = FetchType.LAZY)
    private Client vslaRepresentative;
    @NotNull
    @Column(name = "is_back_dated", columnDefinition = "tinyint(1) default 1")
    private Boolean backDated;
    @NotNull
    @Column(name = "submitted_on_date")
    private Date submittedOnDate;
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "vsla_status")
    private VslaAccountStatus vslaAccountStatus;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAwamoId() {
        return awamoId;
    }

    public void setAwamoId(String awamoId) {
        this.awamoId = awamoId;
    }

    public Office getOfficeId() {
        return officeId;
    }

    public void setOfficeId(Office officeId) {
        this.officeId = officeId;
    }

    public VslaAccount getParentVsla() {
        return parentVsla;
    }

    public void setParentVsla(VslaAccount parentVsla) {
        this.parentVsla = parentVsla;
    }

    public Staff getResponsibleLoanOfficer() {
        return responsibleLoanOfficer;
    }

    public void setResponsibleLoanOfficer(Staff responsibleLoanOfficer) {
        this.responsibleLoanOfficer = responsibleLoanOfficer;
    }

    public Client getVslaRepresentative() {
        return vslaRepresentative;
    }

    public void setVslaRepresentative(Client vslaRepresentative) {
        this.vslaRepresentative = vslaRepresentative;
    }

    public Boolean getBackDated() {
        return backDated;
    }

    public void setBackDated(Boolean backDated) {
        this.backDated = backDated;
    }

    public Date getSubmittedOnDate() {
        return submittedOnDate;
    }

    public void setSubmittedOnDate(Date submittedOnDate) {
        this.submittedOnDate = submittedOnDate;
    }

    public VslaAccountStatus getVslaAccountStatus() {
        return vslaAccountStatus;
    }

    public void setVslaAccountStatus(VslaAccountStatus vslaAccountStatus) {
        this.vslaAccountStatus = vslaAccountStatus;
    }

    public boolean isNotActive() {
        return !isActive();
    }

    public boolean isActive() {
        return VslaAccountStatus.ACTIVE.equals(this.vslaAccountStatus);
    }
}
