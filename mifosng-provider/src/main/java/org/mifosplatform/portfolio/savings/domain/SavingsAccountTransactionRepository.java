/**
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package org.mifosplatform.portfolio.savings.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SavingsAccountTransactionRepository extends JpaRepository<SavingsAccountTransaction, Long>,
        JpaSpecificationExecutor<SavingsAccountTransaction> {

    SavingsAccountTransaction findOneByIdAndSavingsAccountId(Long transactionId, Long savingsId);

    @Query("select distinct t from SavingsAccountTransaction as t " +
            "left join fetch t.paymentDetail " +
            "left join fetch t.savingsAccount " +
            "left join fetch t.savingsAccountChargesPaid " +
            "where (t.savingsAccount.id = :savingsAccountId) order by t.dateOf, t.createdDate, t.id  ")
    List<SavingsAccountTransaction> findSavingsAccTransactions(@Param("savingsAccountId") long savingsAccountId);

}