/**
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package org.mifosplatform.portfolio.savings.domain;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;

import java.util.Comparator;

/**
 * Sort savings account transactions by transaction date and transaction type
 * placing
 */
public class SavingsAccountTransactionComparator implements Comparator<SavingsAccountTransaction> {

    @Override
    public int compare(final SavingsAccountTransaction o1, final SavingsAccountTransaction o2) {
        return ComparisonChain.start()
                .compare(o1.transactionLocalDate(), o2.transactionLocalDate())
                .compare(o1.createdDate(), o2.createdDate(), Ordering.natural().nullsLast())
                .compare(o1.getId(), o2.getId(), Ordering.natural().nullsLast())
                .result();
    }
}