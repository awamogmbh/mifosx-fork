package org.mifosplatform.portfolio.savings.exception;

import org.mifosplatform.infrastructure.core.exception.AbstractPlatformResourceNotFoundException;

public class VslaAccountNotFoundException extends AbstractPlatformResourceNotFoundException {

    public VslaAccountNotFoundException(final Long id) {
        super("error.msg.vsla.id.invalid", "VslaAccount with identifier " + id + " does not exist", id);
    }

}
