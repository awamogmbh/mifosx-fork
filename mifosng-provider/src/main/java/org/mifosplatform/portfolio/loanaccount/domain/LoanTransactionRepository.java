/**
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package org.mifosplatform.portfolio.loanaccount.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface LoanTransactionRepository extends JpaRepository<LoanTransaction, Long>, JpaSpecificationExecutor<LoanTransaction> {
    // no added behaviour

    //we load repayment_at_disbursements and disbursments since they are not always replayed
    @Query("select distinct t from LoanTransaction as t " +
            "left join fetch t.loanChargesPaid " +
            "left join fetch t.loanTransactionToRepaymentScheduleMappings " +
            "where (t.reversed = false or t.typeOf in  (5,1)) and t.loan.id = :loanId order by t.dateOf , t.id  ")
    List<LoanTransaction> findNonReversedTransactions(@Param("loanId") long loanId);
//select t from org.mifosplatform.portfolio.loanaccount.domain.LoanTransaction as t left join fetch t.loanChargesPaid left t.loanTransactionToRepaymentScheduleMappings where t.reversed = false and t.loan.id = :loanId
}