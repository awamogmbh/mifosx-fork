/**
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package org.mifosplatform.portfolio.loanaccount.domain;

import org.apache.commons.lang3.Validate;
import org.joda.time.LocalDate;
import org.mifosplatform.organisation.monetary.domain.MonetaryCurrency;
import org.mifosplatform.organisation.monetary.domain.Money;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * A wrapper around loan schedule related data exposing needed behaviour by
 * loan.
 */
public class LoanRepaymentScheduleProcessingWrapper {

    public void reprocess(final MonetaryCurrency currency, final LocalDate disbursementDate,
            final List<LoanRepaymentScheduleInstallment> repaymentPeriods, final Set<LoanCharge> loanCharges) {//BM Reprocessing of repayment sheduled in case of a backdate repayment

        Money totalInterest = Money.zero(currency);
        Money totalPrincipal = Money.zero(currency);
        for (final LoanRepaymentScheduleInstallment installment : repaymentPeriods) {
            totalInterest = totalInterest.plus(installment.getInterestCharged(currency));
            totalPrincipal = totalPrincipal.plus(installment.getPrincipal(currency));
        }
        LocalDate startDate = disbursementDate;
        for (final LoanRepaymentScheduleInstallment period : repaymentPeriods) {

            final Money feeChargesDueForRepaymentPeriod = cumulativeFeeChargesDueWithin(startDate, period.getDueDate(), loanCharges,
                    currency, period, repaymentPeriods.size(), totalPrincipal, totalInterest, true);
            final Money feeChargesWaivedForRepaymentPeriod = cumulativeFeeChargesWaivedWithin(startDate, period.getDueDate(), loanCharges,
                    currency, true);
            final Money feeChargesWrittenOffForRepaymentPeriod = cumulativeFeeChargesWrittenOffWithin(startDate, period.getDueDate(),
                    loanCharges, currency, true);

            final Money penaltyChargesDueForRepaymentPeriod = cumulativePenaltyChargesDueWithin(startDate, period.getDueDate(),
                    loanCharges, currency, period);
            final Money penaltyChargesWaivedForRepaymentPeriod = cumulativePenaltyChargesWaivedWithin(startDate, period.getDueDate(),
                    loanCharges, currency, period);
            final Money penaltyChargesWrittenOffForRepaymentPeriod = cumulativePenaltyChargesWrittenOffWithin(startDate,
                    period.getDueDate(), loanCharges, currency, period);

            period.updateChargePortion(feeChargesDueForRepaymentPeriod, feeChargesWaivedForRepaymentPeriod,//BM at this point we lose the penalty amount
                    feeChargesWrittenOffForRepaymentPeriod, penaltyChargesDueForRepaymentPeriod, penaltyChargesWaivedForRepaymentPeriod,
                    penaltyChargesWrittenOffForRepaymentPeriod);

            startDate = period.getDueDate();
        }
    }

    private Money cumulativeFeeChargesDueWithin(final LocalDate periodStart, final LocalDate periodEnd, final Set<LoanCharge> loanCharges,
            final MonetaryCurrency monetaryCurrency, LoanRepaymentScheduleInstallment period, int numberOfRepayments,
            final Money totalPrincipal, final Money totalInterest, boolean isInstallmentChargeApplicable) {

        Money cumulative = Money.zero(monetaryCurrency);

        for (final LoanCharge loanCharge : loanCharges) {
            if (loanCharge.isFeeCharge() && !loanCharge.isDueAtDisbursement()) {
                if (loanCharge.isInstalmentFee() && isInstallmentChargeApplicable) {
                    if (loanCharge.getChargeCalculation().isPercentageBased()) {
                        BigDecimal amount = BigDecimal.ZERO;
                        if (loanCharge.getChargeCalculation().isPercentageOfAmountAndInterest()) {
                            amount = amount.add(period.getPrincipal(monetaryCurrency).getAmount()).add(
                                    period.getInterestCharged(monetaryCurrency).getAmount());
                        } else if (loanCharge.getChargeCalculation().isPercentageOfInterest()) {
                            amount = amount.add(period.getInterestCharged(monetaryCurrency).getAmount());
                        } else {
                            amount = amount.add(period.getPrincipal(monetaryCurrency).getAmount());
                        }
                        BigDecimal loanChargeAmt = amount.multiply(loanCharge.getPercentage()).divide(BigDecimal.valueOf(100));
                        cumulative = cumulative.plus(loanChargeAmt);
                    } else {
                        cumulative = cumulative.plus(loanCharge.amount().divide(BigDecimal.valueOf(numberOfRepayments)));
                    }
                } else if (loanCharge.isOverdueInstallmentCharge()
                        && loanCharge.isDueForCollectionFromAndUpToAndIncluding(periodStart, periodEnd)
                        && loanCharge.getChargeCalculation().isPercentageBased()) {
                    cumulative = cumulative.plus(loanCharge.chargeAmount());
                } else if (loanCharge.isDueForCollectionFromAndUpToAndIncluding(periodStart, periodEnd)
                        && loanCharge.getChargeCalculation().isPercentageBased()) {
                    BigDecimal amount = BigDecimal.ZERO;
                    if (loanCharge.getChargeCalculation().isPercentageOfAmountAndInterest()) {
                        amount = amount.add(totalPrincipal.getAmount()).add(totalInterest.getAmount());
                    } else if (loanCharge.getChargeCalculation().isPercentageOfInterest()) {
                        amount = amount.add(totalInterest.getAmount());
                    } else {
                        amount = amount.add(totalPrincipal.getAmount());
                    }
                    BigDecimal loanChargeAmt = amount.multiply(loanCharge.getPercentage()).divide(BigDecimal.valueOf(100));
                    cumulative = cumulative.plus(loanChargeAmt);
                } else if (loanCharge.isDueForCollectionFromAndUpToAndIncluding(periodStart, periodEnd)) {
                    cumulative = cumulative.plus(loanCharge.amount());
                }
            }
        }

        return cumulative;
    }

    private Money cumulativeFeeChargesWaivedWithin(final LocalDate periodStart, final LocalDate periodEnd,
            final Set<LoanCharge> loanCharges, final MonetaryCurrency currency, boolean isInstallmentChargeApplicable) {

        Money cumulative = Money.zero(currency);

        for (final LoanCharge loanCharge : loanCharges) {
            if (loanCharge.isFeeCharge() && !loanCharge.isDueAtDisbursement()) {
                if (loanCharge.isInstalmentFee() && isInstallmentChargeApplicable) {
                    LoanInstallmentCharge loanChargePerInstallment = loanCharge.getInstallmentLoanCharge(periodEnd);
                    if (loanChargePerInstallment != null) {
                        cumulative = cumulative.plus(loanChargePerInstallment.getAmountWaived(currency));
                    }
                } else if (loanCharge.isDueForCollectionFromAndUpToAndIncluding(periodStart, periodEnd)) {
                    cumulative = cumulative.plus(loanCharge.getAmountWaived(currency));
                }
            }
        }

        return cumulative;
    }

    private Money cumulativeFeeChargesWrittenOffWithin(final LocalDate periodStart, final LocalDate periodEnd,
            final Set<LoanCharge> loanCharges, final MonetaryCurrency currency, boolean isInstallmentChargeApplicable) {

        Money cumulative = Money.zero(currency);

        for (final LoanCharge loanCharge : loanCharges) {
            if (loanCharge.isFeeCharge() && !loanCharge.isDueAtDisbursement()) {
                if (loanCharge.isInstalmentFee() && isInstallmentChargeApplicable) {
                    LoanInstallmentCharge loanChargePerInstallment = loanCharge.getInstallmentLoanCharge(periodEnd);
                    if (loanChargePerInstallment != null) {
                        cumulative = cumulative.plus(loanChargePerInstallment.getAmountWrittenOff(currency));
                    }
                } else if (loanCharge.isDueForCollectionFromAndUpToAndIncluding(periodStart, periodEnd)) {
                    cumulative = cumulative.plus(loanCharge.getAmountWrittenOff(currency));
                }
            }
        }

        return cumulative;
    }

    public Money cumulativePenaltyChargesDueWithin(final LocalDate periodStart, final LocalDate periodEnd,
                                                    final Set<LoanCharge> loanCharges, final MonetaryCurrency currency,
                                                    LoanRepaymentScheduleInstallment period) {

        Money cumulative = Money.zero(currency);
        final List<LoanCharge> penaltiesForInstallment = getActiveOverduePenaltiesForInstallment(loanCharges, period, periodStart, periodEnd);
        for (final LoanCharge loanCharge : penaltiesForInstallment) {
            cumulative = cumulative.plus(loanCharge.amount());
        }
        return cumulative;
    }

    public Money cumulativePenaltyChargesWaivedWithin(final LocalDate periodStart, final LocalDate periodEnd,
                                                       final Set<LoanCharge> loanCharges, final MonetaryCurrency currency,
                                                       final LoanRepaymentScheduleInstallment period) {

        Money cumulative = Money.zero(currency);
        final List<LoanCharge> penaltiesForInstallment = getActiveOverduePenaltiesForInstallment(loanCharges, period, periodStart, periodEnd);
        for (final LoanCharge loanCharge : penaltiesForInstallment) {
            cumulative = cumulative.plus(loanCharge.getAmountWaived(currency));
        }
        return cumulative;
    }

    public Money cumulativePenaltyChargesWrittenOffWithin(final LocalDate periodStart, final LocalDate periodEnd,
                                                          final Set<LoanCharge> loanCharges, final MonetaryCurrency currency,
                                                          LoanRepaymentScheduleInstallment period) {

        Money cumulative = Money.zero(currency);
        final List<LoanCharge> activePenalties = getActiveOverduePenaltiesForInstallment(loanCharges, period, periodStart, periodEnd);
        for (final LoanCharge loanCharge : activePenalties) {
            cumulative = cumulative.plus(loanCharge.getAmountWrittenOff(currency));
        }
        return cumulative;
    }


    public List<LoanCharge> getActiveOverduePenaltiesForInstallment(Collection<LoanCharge> charges, LoanRepaymentScheduleInstallment installment, LocalDate periodStart, LocalDate periodEnd) {

        List<LoanCharge> applicableCharges = new ArrayList<>();

        for (LoanCharge loanCharge : charges) {

            final boolean activePenalty = isActivePenalty(loanCharge);

            if (activePenalty) {
                checkThatItIsASupportedAwamoCharge(loanCharge);

                final LoanRepaymentScheduleInstallment installmentForCharge = extractInstallment(loanCharge);

                if (isSameInstallment(installment, installmentForCharge)
                        && loanCharge.isDueForCollectionFromAndUpToAndIncluding(periodStart, periodEnd)) {
                    applicableCharges.add(loanCharge);
                }
            }
        }
        return applicableCharges;
    }

    public void checkThatItIsASupportedAwamoCharge(LoanCharge loanCharge) {
        if (!loanCharge.isAwamoPenalty()) {
            throw new UnsupportedOperationException("Penalty charges are not supported of Calculation type [" + loanCharge.getChargeCalculation()
                    + "] and Charge time [" + loanCharge.getChargeTimeType()
                    + "] On Charge Id [" + loanCharge.getId() + "]");
        }
    }

    boolean isActivePenalty(LoanCharge charge) {
        return charge.isPenaltyCharge() && charge.isActive();
    }

    LoanRepaymentScheduleInstallment extractInstallment(LoanCharge charge) {
        final LoanOverdueInstallmentCharge overdueInstallmentCharge = charge.getOverdueInstallmentCharge();
        Validate.notNull(overdueInstallmentCharge, "Overdue installment charge is null for penalty charge: " + charge.getId());
        return overdueInstallmentCharge.getInstallment();
    }

    boolean isSameInstallment(LoanRepaymentScheduleInstallment installment, LoanRepaymentScheduleInstallment installment1) {
        return installment1.getId().equals(installment.getId());
    }

}