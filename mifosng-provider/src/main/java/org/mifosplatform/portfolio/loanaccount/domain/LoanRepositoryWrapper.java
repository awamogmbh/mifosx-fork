/**
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package org.mifosplatform.portfolio.loanaccount.domain;

import org.mifosplatform.infrastructure.core.service.RemovedItemsList;
import org.mifosplatform.portfolio.loanaccount.exception.LoanNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 *
 * All loan repository saves should come through here to allow management of embedded transactions
 * <p>
 * Wrapper for {@link LoanRepository} that adds NULL checking and Error handling
 * capabilities
 * </p>
 */
@Service
public class LoanRepositoryWrapper {

    private final LoanRepository repository;
    private final LoanChargeRepository loanChargeRepository;
    private final LoanTransactionRepository loanTransactionRepository;

    @Autowired
    public LoanRepositoryWrapper(final LoanRepository repository,
                                 LoanChargeRepository loanChargeRepository,
                                 LoanTransactionRepository loanTransactionRepository) {
        this.repository = repository;
        this.loanTransactionRepository = loanTransactionRepository;
        this.loanChargeRepository = loanChargeRepository;
    }

    public Loan findOneWithNotFoundDetection(final Long id) {
        final Loan loan = this.repository.findOne(id);
        if (loan == null) {
            throw new LoanNotFoundException(id);
        }
        return loan;
    }

    public Collection<Loan> findActiveLoansByLoanIdAndGroupId(Long clientId, Long groupId) {
        final Collection<Integer> loanStatuses = new ArrayList<>(Arrays.asList(LoanStatus.SUBMITTED_AND_PENDING_APPROVAL.getValue(),
                LoanStatus.APPROVED.getValue(), LoanStatus.ACTIVE.getValue(), LoanStatus.OVERPAID.getValue()));

        return this.repository.findByClientIdAndGroupIdAndLoanStatus(clientId, groupId, loanStatuses);
    }


    public Loan save(Loan entity) {
        Loan saved = repository.save(entity);
        saveTransactions(entity);
        return saved;
    }

    public Loan saveAndFlush(Loan entity) {
        Loan saved = save(entity);
        flush();
        return saved;
    }


    public List<Loan> save(Iterable<Loan> entities) {
        List<Loan> result = new ArrayList<>();

        if (entities == null) {
            return result;
        }

        for (Loan entity : entities) {
            Loan saved = save(entity);
            result.add(saved);
        }

        return result;
    }


    private void saveTransactions(Loan loan) {
        List<LoanTransaction> deletedTransactions = loan.deletedTransactions();
        for (LoanTransaction t : deletedTransactions) {
            //some times transaction are removed from the list so that they are saved individually
            //so in this case make sure you only delete transactions with an id
            mayBeDelete(t);
        }

        List<LoanTransaction> loanTransactions1 = loan.getLoanTransactions();
        for (LoanTransaction t : loanTransactions1) {
            loanTransactionRepository.save(t);
            //in case this tx is reverse flush the session to release the external id
            if (t.isReversed()) flush();
        }

        List<LoanCharge> deletedCharges = loan.deletedCharges();
        for (LoanCharge c : deletedCharges) {
            if (c.getId() != null) loanChargeRepository.delete(c);
        }

        RemovedItemsList<LoanCharge> loanCharges = loan.chargesAsList();
        for (LoanCharge c : loanCharges) {
            loanChargeRepository.save(c);
        }

    }

    private void mayBeDelete(LoanTransaction t) {
        if (t.getId() != null) loanTransactionRepository.delete(t);
    }

    public List<Loan> getGroupLoansDisbursedAfter(Date disbursementDate, Long groupId, Integer loanType) {
        return repository.getGroupLoansDisbursedAfter(disbursementDate, groupId, loanType);
    }


    public List<Loan> getClientOrJLGLoansDisbursedAfter(Date disbursementDate, Long clientId) {
        return repository.getClientOrJLGLoansDisbursedAfter(disbursementDate, clientId);
    }


    public Integer getMaxGroupLoanCounter(Long groupId, Integer loanType) {
        return repository.getMaxGroupLoanCounter(groupId, loanType);
    }


    public Integer getMaxGroupLoanProductCounter(Long productId, Long groupId, Integer loanType) {
        return repository.getMaxGroupLoanProductCounter(productId, groupId, loanType);
    }


    public Integer getMaxClientOrJLGLoanCounter(Long clientId) {
        return repository.getMaxClientOrJLGLoanCounter(clientId);
    }


    public Integer getMaxClientOrJLGLoanProductCounter(Long productId, Long clientId) {
        return repository.getMaxClientOrJLGLoanProductCounter(productId, clientId);
    }


    public List<Loan> getGroupLoansToUpdateLoanCounter(Integer loanCounter, Long groupId, Integer groupLoanType) {
        return repository.getGroupLoansToUpdateLoanCounter(loanCounter, groupId, groupLoanType);
    }


    public List<Loan> getClientOrJLGLoansToUpdateLoanCounter(Integer loanCounter, Long clientId) {
        return repository.getClientOrJLGLoansToUpdateLoanCounter(loanCounter, clientId);
    }


    public List<Loan> getGroupLoansToUpdateLoanProductCounter(Integer loanProductCounter, Long groupId, Integer groupLoanType) {
        return repository.getGroupLoansToUpdateLoanProductCounter(loanProductCounter, groupId, groupLoanType);
    }


    public List<Loan> getClientLoansToUpdateLoanProductCounter(Integer loanProductCounter, Long clientId) {
        return repository.getClientLoansToUpdateLoanProductCounter(loanProductCounter, clientId);
    }


    public List<Loan> findByClientIdAndGroupId(Long clientId, Long groupId) {
        return repository.findByClientIdAndGroupId(clientId, groupId);
    }


    public List<Loan> findByClientIdAndGroupIdAndLoanStatus(Long clientId, Long groupId, Collection<Integer> loanStatuses) {
        return repository.findByClientIdAndGroupIdAndLoanStatus(clientId, groupId, loanStatuses);
    }


    public List<Loan> findLoanByClientId(Long clientId) {
        return repository.findLoanByClientId(clientId);
    }


    public List<Loan> findByGroupId(Long groupId) {
        return repository.findByGroupId(groupId);
    }


    public List<Loan> findByIdsAndLoanStatusAndLoanType(Collection<Long> ids, Collection<Integer> loanStatuses, Collection<Integer> loanTypes) {
        return repository.findByIdsAndLoanStatusAndLoanType(ids, loanStatuses, loanTypes);
    }


    public List<Long> getLoansDisbursedAfter(Date disbursalDate) {
        return repository.getLoansDisbursedAfter(disbursalDate);
    }


    public List<Loan> findByClientOfficeIdsAndLoanStatus(Collection<Long> officeIds, Collection<Integer> loanStatuses) {
        return repository.findByClientOfficeIdsAndLoanStatus(officeIds, loanStatuses);
    }


    public List<Loan> findByGroupOfficeIdsAndLoanStatus(Collection<Long> officeIds, Collection<Integer> loanStatuses) {
        return repository.findByGroupOfficeIdsAndLoanStatus(officeIds, loanStatuses);
    }


    public List<Long> findActiveLoansLoanProductIdsByClient(Long clientId, Integer loanStatus) {
        return repository.findActiveLoansLoanProductIdsByClient(clientId, loanStatus);
    }


    public List<Long> findActiveLoansLoanProductIdsByGroup(Long groupId, Integer loanStatus) {
        return repository.findActiveLoansLoanProductIdsByGroup(groupId, loanStatus);
    }


    public boolean doNonClosedLoanAccountsExistForClient(Long clientId) {
        return repository.doNonClosedLoanAccountsExistForClient(clientId);
    }


    public boolean doNonClosedLoanAccountsExistForProduct(Long productId) {
        return repository.doNonClosedLoanAccountsExistForProduct(productId);
    }


    public List<Loan> findAll() {
        return repository.findAll();
    }


    public List<Loan> findAll(Sort sort) {
        return repository.findAll(sort);
    }


    public List<Loan> findAll(Iterable<Long> longs) {
        return repository.findAll(longs);
    }


    public void flush() {
        repository.flush();
    }


    public void deleteInBatch(Iterable<Loan> entities) {
        repository.deleteInBatch(entities);
    }


    public void deleteAllInBatch() {
        repository.deleteAllInBatch();
    }


    public Loan getOne(Long aLong) {
        return repository.getOne(aLong);
    }


    public Page<Loan> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }


    public Loan findOne(Long aLong) {
        return repository.findOne(aLong);
    }


    public boolean exists(Long aLong) {
        return repository.exists(aLong);
    }


    public long count() {
        return repository.count();
    }


    public void delete(Long aLong) {
        repository.delete(aLong);
    }


    public void delete(Loan entity) {
        repository.delete(entity);
    }


    public void delete(Iterable<? extends Loan> entities) {
        repository.delete(entities);
    }


    public void deleteAll() {
        repository.deleteAll();
    }


    public Loan findOne(Specification<Loan> spec) {
        return repository.findOne(spec);
    }


    public List<Loan> findAll(Specification<Loan> spec) {
        return repository.findAll(spec);
    }


    public Page<Loan> findAll(Specification<Loan> spec, Pageable pageable) {
        return repository.findAll(spec, pageable);
    }


    public List<Loan> findAll(Specification<Loan> spec, Sort sort) {
        return repository.findAll(spec, sort);
    }


    public long count(Specification<Loan> spec) {
        return repository.count(spec);
    }


}