/**
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package org.mifosplatform.portfolio.loanaccount.domain;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;

import java.util.Comparator;

/**
 * Sort loan transactions by transaction date, created date and transaction type
 * placing
 */
public class LoanTransactionComparator implements Comparator<LoanTransaction> {


    @Override
    public int compare(LoanTransaction o1, LoanTransaction o2) {
        return ComparisonChain.start()
                .compare(o1.getTransactionDate(), o2.getTransactionDate())
                .compare(o1.getCreatedDateTime(), o2.getCreatedDateTime(), Ordering.natural().nullsLast())
                .compare(waiveWeight(o1), waiveWeight(o2))
                .compare(o1.getId(), o2.getId(), Ordering.natural().nullsLast())
                .result();
    }

    private static int waiveWeight(LoanTransaction t1) {
        LoanTransactionType typeOf = t1.getTypeOf();
        switch (typeOf) {
            case WAIVE_INTEREST:
                return 1;
            case WAIVE_CHARGES:
                return 2;
            default:
                return 3;
        }
    }

}
