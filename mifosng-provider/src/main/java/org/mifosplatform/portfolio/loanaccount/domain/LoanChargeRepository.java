/**
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package org.mifosplatform.portfolio.loanaccount.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface LoanChargeRepository extends JpaRepository<LoanCharge, Long>, JpaSpecificationExecutor<LoanCharge> {
    // no added behaviour

    @Query("select distinct lc from LoanCharge lc " +
            "left join fetch lc.loanInstallmentCharge " +
            "left join fetch lc.overdueInstallmentCharge " +
            "left join fetch lc.loanTrancheDisbursementCharge " +
            " where lc.loan.id = :loanId and lc.active = true")
    List<LoanCharge> findByLoanId(@Param("loanId") Long loanId);
}
