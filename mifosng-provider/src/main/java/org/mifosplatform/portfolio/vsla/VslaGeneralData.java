package org.mifosplatform.portfolio.vsla;

public class VslaGeneralData {
    private final Long id;
    private final String name;
    private final String externalId;
    private final Long officeId;
    private final Long memberId;
    private final String memberExternalId;
    private final String memberName;


    public VslaGeneralData(Long id, String name, String externalId, Long officeId, Long memberId, String memberExternalId, String memberName) {
        this.id = id;
        this.name = name;
        this.externalId = externalId;
        this.officeId = officeId;
        this.memberId = memberId;
        this.memberExternalId = memberExternalId;
        this.memberName = memberName;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getExternalId() {
        return externalId;
    }

    public Long getOfficeId() {
        return officeId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public String getMemberExternalId() {
        return memberExternalId;
    }

    public String getMemberName() {
        return memberName;
    }
}
