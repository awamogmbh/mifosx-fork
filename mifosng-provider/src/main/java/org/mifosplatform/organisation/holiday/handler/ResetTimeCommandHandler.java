/**
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package org.mifosplatform.organisation.holiday.handler;

import org.mifosplatform.commands.annotation.CommandType;
import org.mifosplatform.commands.handler.NewCommandSourceHandler;
import org.mifosplatform.infrastructure.core.api.JsonCommand;
import org.mifosplatform.infrastructure.core.data.CommandProcessingResult;
import org.mifosplatform.infrastructure.core.data.CommandProcessingResultBuilder;
import org.mifosplatform.infrastructure.core.service.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@CommandType(entity = "TIME", action = "RESET")
public class ResetTimeCommandHandler implements NewCommandSourceHandler {


    @Transactional
    @Override
    public CommandProcessingResult processCommand(final JsonCommand command) {

        DateUtils.resetTime();

        return new CommandProcessingResultBuilder()
                .withCommandId(command.commandId())
                .withResourceIdAsString(
                        DateUtils.formatReadableDate(
                                DateUtils.now().toDate()))
                .build();
    }
}
