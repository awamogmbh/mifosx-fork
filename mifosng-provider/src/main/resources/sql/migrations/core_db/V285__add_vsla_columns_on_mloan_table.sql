ALTER TABLE `m_loan`
	ADD COLUMN `vsla_id` BIGINT(20) NULL DEFAULT NULL ,
	ADD CONSTRAINT `FK_m_loan_vsla_id` FOREIGN KEY (`vsla_id`) REFERENCES `m_vsla` (`id`);

ALTER TABLE `m_loan`
	ADD COLUMN `vsla_member_id` BIGINT(20) NULL DEFAULT NULL ,
	ADD CONSTRAINT `FK_m_loan_vsla_member_id` FOREIGN KEY (`vsla_member_id`) REFERENCES `m_vsla_member` (`id`);