ALTER TABLE `m_savings_account`
	ADD COLUMN `vsla_id` BIGINT(20) NULL DEFAULT NULL ,
	ADD CONSTRAINT `FK_m_savings_account_vsla_id` FOREIGN KEY (`vsla_id`) REFERENCES `m_vsla` (`id`);

ALTER TABLE `m_portfolio_command_source`
	ADD COLUMN `vsla_id` BIGINT(20) NULL DEFAULT NULL ,
	ADD CONSTRAINT `FK_m_portfolio_command_source_vsla_id` FOREIGN KEY (`vsla_id`) REFERENCES `m_vsla` (`id`);