INSERT IGNORE INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ('time', 'MOVE_TIME', 'TIME', 'MOVE', 0);
INSERT IGNORE INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ('time', 'RESET_TIME', 'TIME', 'RESET', 0);
