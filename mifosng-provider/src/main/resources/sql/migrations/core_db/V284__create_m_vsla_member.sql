
create table `m_vsla_member`
(
    `id`                   bigint auto_increment
        primary key,
    `client_id`            bigint                              not null,
    `can_withdraw`         bit       default b'0'              not null,
    `can_receive_ussd_sms` bit       default b'0'              not null,
    `vsla_id`              bigint                              not null,
    `is_active`            bit       default b'0'              not null,
    `activated_on`         timestamp default CURRENT_TIMESTAMP not null,
    `activated_by`         bigint                              null,
    `is_disabled`          bit       default b'0'              null,
    `disabled_on`          timestamp default CURRENT_TIMESTAMP null,
    `disabled_by`          bigint                              null,
    `disabled_note`        longtext                            null,
    `created_on`           timestamp default CURRENT_TIMESTAMP not null,
    `modified_on`          timestamp default CURRENT_TIMESTAMP null,
    `created_by`           bigint                              null,
    `modified_by`          bigint                              null,
    constraint `FK_m_vsla_member_activated_on`
        foreign key (`activated_by`) references `m_appuser` (`id`),
    constraint `FK_m_vsla_member_client_id`
        foreign key (`client_id`) references `m_client` (`id`),
    constraint `FK_m_vsla_member_created_by`
        foreign key (`created_by`) references `m_appuser` (`id`),
    constraint `FK_m_vsla_member_disabled_by`
        foreign key (`disabled_by`) references `m_appuser` (`id`),
    constraint `FK_m_vsla_member_modified_by`
        foreign key (`modified_by`) references `m_appuser` (`id`),
    constraint `FK_m_vsla_member_vsla_id`
        foreign key (`vsla_id`) references `m_vsla` (`id`)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;
