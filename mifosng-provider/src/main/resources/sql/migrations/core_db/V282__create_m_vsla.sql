create table `m_vsla`
(
    `id`                        bigint auto_increment
        primary key,
    `name`                      varchar(255)                        not null,
    `awamo_id`                  varchar(10)                         not null,
    `office_id`                 bigint                              not null,
    `responsible_loan_officer`  bigint                              not null,
    `vsla_representative`       bigint                              not null,
    `is_back_dated`             bit       default b'0'              not null,
    `submitted_on_date`         datetime                            not null,
    `vsla_status`               varchar(50)                         not null,
    `created_on`                timestamp default CURRENT_TIMESTAMP not null,
    `modified_on`               timestamp default CURRENT_TIMESTAMP null,
    `created_by`                bigint                              null,
    `modified_by`               bigint                              null,
    `number_of_members_derived` int                                 null,
    constraint `FK_m_vsla_created_by`
        foreign key (`created_by`) references `m_appuser` (`id`),
    constraint `FK_m_vsla_modified_by`
        foreign key (`modified_by`) references `m_appuser` (`id`),
    constraint `FK_m_vsla_office_id`
        foreign key (`office_id`) references `m_office` (`id`),
    constraint `FK_m_vsla_representative`
        foreign key (`vsla_representative`) references `m_client` (`id`),
    constraint `FK_m_vsla_responsible_loan_officer`
        foreign key (`responsible_loan_officer`) references `m_staff` (`id`)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;
