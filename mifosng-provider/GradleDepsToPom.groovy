@groovy.lang.Grab("io.github.kayr:fuzzy-csv:1.7.1")

import groovy.xml.MarkupBuilder
import fuzzycsv.FuzzyCSVTable
import static fuzzycsv.FuzzyStaticApi.*

def file = args[0]

def text = (file as File).text

def regex = /[a-zA-Z\-_0-9]+(\.[a-zA-Z\-_0-9]++)*:[a-zA-Z-_0-9.]+:([0-9]+(\.[0-9]+)*)[a-zA-Z.\-0-9]*/


def data = FuzzyCSVTable.withHeader('groupId', 'artifactId', 'version')


text.findAll(regex) {

    def coordinates = it[0]

    def (group, artifact, verion) = coordinates.split(':')


    data.addRecordArr(group, artifact, verion)
}


data = data
        .distinct()
        .modify {
            set {
                it.version = new Version(it.version)
            }
        }
        .sort('groupId', 'artifactId', 'version')
        .reverse()
        .distinctBy('groupId', 'artifactId')
        .printTable()
        .addColumn(fx('command') {
            if (it.groupId == 'com.github.kayr')
                "mvn dependency:get -DremoteRepositories=jitpack::default::https://jitpack.io -Dartifact=$it.groupId:$it.artifactId:$it.version"
            else
                "mvn dependency:get -Dartifact=$it.groupId:$it.artifactId:$it.version"
        })


def wr = new StringWriter()
def builder = new MarkupBuilder(wr)


builder.project(
        [xmlns               : "http://maven.apache.org/POM/4.0.0",
         'xmlns:xsi'         : "http://www.w3.org/2001/XMLSchema-instance",
         'xsi:schemaLocation': "http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"]) {

    modelVersion('4.0.0')
    groupId('FakePom')
    artifactId('FakeArtifact')
    version('1.0')


    repositories {
        repository {
            id('central')
            url(' https://repo.maven.apache.org/maven2')
        }
        repository {
            id('jitpack')
            url('https://jitpack.io')
        }
    }

    dependencies {
        data.each { r ->
            dependency {
                groupId(r[0])
                artifactId(r[1])
                version(r[2])
            }
        }

    }
}

println "Writing pom.xml"
new File('pom.xml').text - wr.toString()


def script = data.collect { it.command }.join('\n')
println "Writing download-all.sh"
new File("download-all.sh").text = "#!/bin/bash -x\n$script"

"chmod +x download-all.sh".execute()


public class Version implements Comparable<Version> {

    private String version;

    public final String get() {
        return this.version;
    }

    public Version(String version) {
        if (version == null)
            throw new IllegalArgumentException("Version can not be null");
        // if(!version.matches("[0-9]+(\\.[0-9]+)*"))
        if (!version.matches("([0-9]+(\\.[0-9]+)*)[a-zA-Z.\\-0-9]*"))
            throw new IllegalArgumentException("Invalid version format [" + version + "]");
        this.version = version;
    }

    @Override
    public int compareTo(Version that) {
        if (that == null)
            return 1;
        String[] thisParts = this.get().split("\\.");
        String[] thatParts = that.get().split("\\.");
        int length = Math.max(thisParts.length, thatParts.length);
        for (int i = 0; i < length; i++) {
            int thisPart = i < thisParts.length ?
                    Integer.parseInt(thisParts[i]) : 0;
            int thatPart = i < thatParts.length ?
                    Integer.parseInt(thatParts[i]) : 0;
            if (thisPart < thatPart)
                return -1;
            if (thisPart > thatPart)
                return 1;
        }
        return 0;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that)
            return true;
        if (that == null)
            return false;
        if (this.getClass() != that.getClass())
            return false;
        return this.compareTo((Version) that) == 0;
    }

    String toString() {
        return version;
    }

}
