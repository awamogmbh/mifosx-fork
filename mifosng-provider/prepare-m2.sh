#!/bin/bash -x
# Prepare download of all dependencies to local maven repo

./gradlew dependencies > deps.txt

groovy GradleDepsToPom.groovy deps.txt

./download-all.sh
