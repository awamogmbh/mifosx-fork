Setting the appropriate connection configs in the DB

```mysql
update tenant_server_connections
set pool_initial_size             =0,
    pool_min_idle                 =0,
    pool_max_idle                 =0,
    pool_max_active               =10,
    pool_remove_abandoned_timeout =30,
    pool_suspect_timeout          =0,
    pool_log_abandoned            =0,
    pool_min_evictable_idle_time_millis  = 10*1000
where 1 = 1;
```

Importing into intellij

1. Generate intellij files `./gradlew idea`
2. To Run the application run `org.mifosplatform.ServerApplication`
3. If you get database errors, change the setting
   in `org.mifosplatform.infrastructure.core.boot.db.DataSourceProperties`

**Note:** Sometimes intellij might fail to find the resource files(e.g., certificates) that is because it compiles them
into a different directory. A hack would be to copy them manually into the compile-output folder

### Scenario for meltdown in loans

When a payment is made

- Mifos iterates through the installments paying the required proportion
- if it pays a penalty during the iteration then should also find a matching penalty in the mloancharge table..
- a problem occurs if the amount on the schedule exists but the is no matching penalty on the mloancharge table. This
  situation can be caused by
   - a penalty having more decimal places than the one on the loan therefore a residue stays on the installment but when
     the charge was actually cleared
   - the mapping to the installment of the charges that were paid have different amounts from the ones on the
     installments
   

Loan repayment process
- Penalties and Fees are sometimes recalculated and summarized onto the schedule if reprocessing is required (
  LoanRepaymentScheduleTransactionProcessor.handleTransaction and LoanRepaymentScheduleProcessingWrapper)
  This is done to avoid having a mismatch between the amount on the schedule and the amount on the charges. At this
  point the installments with paid principal and interest are marked as cleared if they do not have charges.
- For each transaction
   - The necessary portions are set on the transaction principal/interest/penalty (
     AbstractLoanRepaymentScheduleTransactionProcessor.processTransaction in
     AbstractLoanRepaymentScheduleTransactionProcessor.handleTransactionAndCharges). This is entirely done using the
     loanRepaymentSchedule
   - Unpaid penalties/charges are fetched (in
     AbstractLoanRepaymentScheduleTransactionProcessor.handleTransactionAndCharges)
   - The proportion paid to the charges/penalties is distributed through the charges in order of the due date (
     AbstractLoanRepaymentScheduleTransactionProcessor.handleTransactionAndCharges)
   - Mifos transaction derived component (e.g. principal portion, interest portion, penalty portion) is set by

Summary:
1. Transaction is inserted
2. Repayment schedule is reprocessed
3. For each transaction
   1. The amount is distributed through the installments. At his point the derived components are set for the transaction.
   2. If the transaction covered a charge then a charge mapping is also created. This is done by covering all charges in order of due date and the charge amount is exhausted. 

Note:
Mifos amounts are distributed the schedule the LoanRepaymentScheduleTransactionProcessor Usually when a charge and
transaction are reprocessed if you make multiple repayments on the same day forcing the second repayment not to be
chronologically the last.

