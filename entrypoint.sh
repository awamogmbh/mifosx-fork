#!/bin/bash
keytool -genkey -noprompt -storepass password -keypass password -keyalg RSA -alias tomcat -validity 3650  -dname "CN=tomcat" -keystore /usr/local/tomcat/keystore.jks
keytool -importkeystore -srcstorepass password  -srckeystore /usr/local/tomcat/keystore.jks -destkeystore /usr/local/tomcat/keystore.jks -deststorepass password -deststoretype pkcs12

printenv | sed 's/^\([a-zA-Z0-9_]*\)=\(.*\)$/export \1="\2"/g' >> /usr/local/tomcat/conf/catalina.properties
#catalina.sh run
