define(['Q', 'underscore', 'mifosX'], function (Q) {
    var components = {
        models: [
            'models.68ba8204'
        ],
        services: [
            'ResourceFactoryProvider',
            'HttpServiceProvider',
            'AuthenticationService',
            'SessionManager',
            'Paginator',
            'UIConfigService'
        ],
        controllers: [
            'controllers.21cad541'
        ],
        filters: [
            'filters.55796a95'
        ],
        directives: [
            'directives.6901368e'
        ]
    };

    return function() {
        var defer = Q.defer();
        require(_.reduce(_.keys(components), function (list, group) {
            return list.concat(_.map(components[group], function (name) {
                return group + "/" + name;
            }));
        }, [
            'routes-initialTasks-webstorage-configuration.9fe16509'
        ]), function(){
            defer.resolve();
        });
        return defer.promise;
    }
});
